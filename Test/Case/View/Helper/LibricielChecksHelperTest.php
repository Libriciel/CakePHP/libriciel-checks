<?php
    /**
     * Code source de la classe LibricielChecksHelperTest.
     *
     * @package LibricielChecks
     * @subpackage Test.Case.View.Helper
     * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
     */
    App::uses('Controller', 'Controller');
    App::uses('View', 'View');
    App::uses('LibricielChecksHelper', 'LibricielChecks.View/Helper');

/**
 * La classe LibricielChecksHelperTest contient les tests unitaires de la classe
 * LibricielChecksHelper.
 *
 * @package LibricielChecks
 * @subpackage Test.Case.View.Helper
 */
class LibricielChecksHelperTest extends CakeTestCase
{
    /**
     * Fixtures utilisés par ces tests unitaires.
     *
     * @var array
     */
    public $fixtures = [
        'core.Apple',
    ];

    public $replacements = [
        '/(check|parent)\-panel-[0-9a-z]+/m' => '\1-panel-0000000000000',
        '/(check)\-group-[0-9a-z]+/m' => '\1-group-0000000000000',

    ];

    protected static function _normalizeXhtml($xhtml)
    {
        $xhtml = preg_replace("/([[:space:]]|\n)+/m", ' ', $xhtml);
        $xhtml = str_replace('> <', '><', $xhtml);

        return trim($xhtml);
    }

    public static function assertEquals($expected, $actual, $message = '', $delta = 0, $maxDepth = 10, $canonicalize = false, $ignoreCase = false)
    {
        if (empty($message)) {
            $message = var_export($actual, true);
        }

        return parent::assertEquals(
            $expected,
            $actual,
            $message,
            $delta,
            $maxDepth,
            $canonicalize,
            $ignoreCase
        );
    }

    public static function assertEqualsXhtml($expected, $actual, $message = '', $delta = 0, $maxDepth = 10, $canonicalize = false, $ignoreCase = false)
    {
        return self::assertEquals(
            self::_normalizeXhtml($expected),
            self::_normalizeXhtml($actual),
            $message,
            $delta,
            $maxDepth,
            $canonicalize,
            $ignoreCase
        );
    }

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $controller = null;
        $this->View = new View($controller);
        $this->Checks = new LibricielChecksHelper($this->View);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->View, $this->Checks);
        parent::tearDown();
    }

    /**
     * Test de la méthode LibricielChecksHelper::checklist()
     *
     * @return void
     */
    public function testChecklist()
    {
        $elements = ['One' => true, 'Two' => false];
        $actual = $this->Checks->checklist($elements, 'drivers');
        $expected = '<ul class="libriciel-checks check drivers"><li class="success text-success">One</li><li class="error text-danger">Two</li></ul>';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test de la méthode LibricielChecksHelper::table()
     *
     * @return void
     */
    public function testTable()
    {
        $elements = ['One' => true, 'Two' => false];
        $actual = $this->Checks->table($elements);
        $expected = '<div class="table-responsive"><table class="table table-striped table-bordered table-hover table-condensed libriciel-checks checks values"><tbody><tr class="col-xs-12"><th>One</th></tr><tr class="col-xs-12"><th>Two</th></tr></tbody></table></div>';
        $this->assertEquals($expected, $actual);

        $elements = ['One' => ['value' => 4, 'message' => 'Foo', 'success' => true], 'Two' => ['value' => 'Bar', 'message' => 'Baz', 'success' => false]];
        $actual = $this->Checks->table($elements);
        $expected = '<div class="table-responsive"><table class="table table-striped table-bordered table-hover table-condensed libriciel-checks checks values"><tbody><tr class="success"><td class="col-xs-1 status success"><span class="fa fa-check text-success"></span> <span class="sr-only">Success</span></td><th class="col-xs-4">One</th><td class="col-xs-3">4</td><td class="col-xs-4">Foo</td></tr><tr class="danger"><td class="col-xs-1 status error"><span class="fa fa-times text-danger"></span> <span class="sr-only">Error</span></td><th class="col-xs-4">Two</th><td class="col-xs-3">Bar</td><td class="col-xs-4">Baz</td></tr></tbody></table></div>';
        $this->assertEquals($expected, $actual);

        $elements = ['One' => ['value' => ['foo' => 'bar'], 'message' => 'Foo', 'success' => true]];
        $actual = $this->Checks->table($elements);
        $expected = '<div class="table-responsive"><table class="table table-striped table-bordered table-hover table-condensed libriciel-checks checks values"><tbody><tr class="success"><td class="col-xs-1 status success"><span class="fa fa-check text-success"></span> <span class="sr-only">Success</span></td><th class="col-xs-4">One</th><td class="col-xs-3">array (
  \'foo\' => \'bar\',
)</td><td class="col-xs-4">Foo</td></tr></tbody></table></div>';
        $this->assertEquals($expected, $actual);
    }

    /**
     * Test de la méthode LibricielChecksHelper::accordion()
     *
     * @return void
     */
    public function testAccordion()
    {
        $actual = $this->Checks->accordion('Titre de l\'accordéon', 'Contenu de l\'accordéon');
        $expected = '<div class="libriciel-checks panel panel-default">
                            <div id="check-panel-5bed3a480b6de-heading" role="tab" class="panel-heading">
                                <h1 class="panel-title">
                                    <a href="#check-panel-5bed3a480b6de-body" role="button" data-toggle="collapse" aria-controls="check-panel-5bed3a480b6de-body" data-parent="#parent-panel-5bed3a480b716" style="display: block;">Titre de l&#039;accordéon</a>
                                </h1>
                            </div>
                            <div id="check-panel-5bed3a480b6de-body" role="tabpanel" aria-labelledby="check-panel-5bed3a480b6de-heading" class="panel-collapse collapse">
                                <div class="panel-body">Contenu de l\'accordéon</div>
                            </div>
                        </div>';
        $this->assertEqualsXhtml(
            preg_replace(array_keys($this->replacements), array_values($this->replacements), $expected),
            preg_replace(array_keys($this->replacements), array_values($this->replacements), $actual)
        );
    }

    /**
     * Test de la méthode LibricielChecksHelper::accordions()
     *
     * @return void
     */
    public function testAccordions()
    {
        $actual = $this->Checks->accordions([
            'Titre 1' => 'Contenu 1',
            'Titre 2' => 'Contenu 2',
            'Titre 3' => 'Contenu 3',
        ]);
        $expected =
        '<div id="check-group-0000000000000" class="libriciel-checks panel-group" role="tablist" aria-multiselectable="true">
                <div class="libriciel-checks panel panel-default">
                    <div id="check-panel-0000000000000-heading" role="tab" class="panel-heading">
                        <h1 class="panel-title">
                            <a href="#check-panel-0000000000000-body" role="button" data-toggle="collapse" aria-controls="check-panel-0000000000000-body" data-parent="#check-group-5c04e6bf518e3" style="display: block;">Titre 1</a>
                        </h1>
                    </div>
                    <div id="check-panel-0000000000000-body" role="tabpanel" aria-labelledby="check-panel-0000000000000-heading" class="panel-collapse collapse">
                        <div class="panel-body">Contenu 1</div>
                    </div>
                </div>
                <div class="libriciel-checks panel panel-default"><div id="check-panel-0000000000000-heading" role="tab" class="panel-heading">
                        <h1 class="panel-title">
                            <a href="#check-panel-0000000000000-body" role="button" data-toggle="collapse" aria-controls="check-panel-0000000000000-body" data-parent="#check-group-5c04e6bf518e3" style="display: block;">Titre 2</a>
                        </h1>
                    </div>
                    <div id="check-panel-0000000000000-body" role="tabpanel" aria-labelledby="check-panel-0000000000000-heading" class="panel-collapse collapse">
                        <div class="panel-body">Contenu 2</div>
                    </div>
                </div>
                <div class="libriciel-checks panel panel-default">
                    <div id="check-panel-0000000000000-heading" role="tab" class="panel-heading">
                        <h1 class="panel-title">
                            <a href="#check-panel-0000000000000-body" role="button" data-toggle="collapse" aria-controls="check-panel-0000000000000-body" data-parent="#check-group-5c04e6bf518e3" style="display: block;">Titre 3</a>
                        </h1>
                    </div>
                    <div id="check-panel-0000000000000-body" role="tabpanel" aria-labelledby="check-panel-0000000000000-heading" class="panel-collapse collapse">
                        <div class="panel-body">Contenu 3</div>
                    </div>
                </div>
            </div>';
        $this->assertEqualsXhtml(
            preg_replace(array_keys($this->replacements), array_values($this->replacements), $expected),
            preg_replace(array_keys($this->replacements), array_values($this->replacements), $actual)
        );
    }

    public function testJavascript()
    {
        $this->markTestIncomplete('Faire les tests de la méthode LibricielChecksHelper::javascript');
    }
}
