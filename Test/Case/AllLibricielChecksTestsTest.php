<?php
    /**
     * AllLibricielChecksTests file
     *
     * @package LibricielChecks
     * @subpackage Test.Case
     */
    CakePlugin::load('LibricielChecks');

/**
 * AllLibricielChecksTests class
 *
 * This test group will run all tests.
 *
 * @package LibricielChecks
 * @subpackage Test.Case
 */
class AllLibricielChecksTests extends PHPUnit_Framework_TestSuite
{
    /**
     * Test suite with all test case files.
     *
     * @return CakeTestSuite
     */
    public static function suite()
    {
        $suite = new CakeTestSuite('All LibricielChecks tests');
        $suite->addTestDirectoryRecursive(dirname(__FILE__) . DS . '..' . DS . 'Case' . DS);

        return $suite;
    }
}
