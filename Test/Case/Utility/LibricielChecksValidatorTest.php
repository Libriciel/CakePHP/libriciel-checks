<?php

App::uses('LibricielChecksValidator', 'LibricielChecks.Utility');

/**
 * La classe LibricielChecksValidatorTest est chargée des tests unitaires de la classe
 * LibricielChecksValidator du plugin LibricielChecks.
 *
 * @package LibricielChecks
 * @subpackage Test.Case
 */
class LibricielChecksValidatorTest extends CakeTestCase
{
    public function dataVersion()
    {
        return [
            ['PHP', '7.2', '7.0', '7.3', ['value' => '7.2', 'success' => true, 'message' => null]],
            ['PHP', '7.0', '7.2', null, ['value' => '7.0', 'success' => false, 'message' => 'The version of PHP should be higher or equal to 7.2']],
            ['PHP', '7.0', '7.2', '7.3', ['value' => '7.0', 'success' => false, 'message' => 'The version of PHP should be higher or equal to 7.2 and also be lower than 7.3']],
        ];
    }

    /**
     * @dataProvider dataVersion
     */
    public function testVersion($software, $actual, $low, $high, $expected)
    {
        $actual = LibricielChecksValidator::version($software, $actual, $low, $high);
        $this->assertEquals($expected, $actual, var_export($actual, true));
    }

    public function dataValidate()
    {
        return [
            [true, 'notBlank', [''], null],
            [null, 'notBlank', [''], 'Validate::notBlank'],
            [true, 'notBlank', ['message' => 'Cannot be blank'], null],
            [null, 'notBlank', ['message' => 'Cannot be blank'], 'Cannot be blank'],
            ['49M', 'bytesizeComparison', ['rule' => ['>=', '50M'], 'message' => 'Veuillez entrer une valeur %s %s', 'allowEmpty' => false], 'Veuillez entrer une valeur >= 50M'],
            [null, 'bytesizeComparison', ['rule' => ['>=', '50M'], 'message' => 'Veuillez entrer une valeur %s %s', 'allowEmpty' => false], 'Veuillez entrer une valeur >= 50M'],
        ];
    }

    /**
     * @dataProvider dataValidate
     */
    public function testValidate($value, $rule, $ruleParams, $expected)
    {
        $actual = LibricielChecksValidator::validate($value, $rule, $ruleParams);
        $this->assertEquals($expected, $actual, var_export($actual, true));
    }
}
