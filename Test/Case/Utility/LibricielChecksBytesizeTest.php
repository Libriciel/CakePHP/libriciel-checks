<?php

App::uses('LibricielChecksBytesize', 'LibricielChecks.Utility');

/**
 * La classe LibricielChecksBytesizeTest est chargée des tests unitaires de la classe
 * LibricielChecksBytesize du plugin LibricielChecks.
 *
 * @package LibricielChecks
 * @subpackage Test.Case
 */
class LibricielChecksBytesizeTest extends CakeTestCase
{
    public function dataToHuman()
    {
        return [
            [0, '0.00 KB'],
            [512, '0.50 KB'],
            [1024, '1.00 KB'],
            [1024 * 1024, '1.00 MB'],
            [1024 * 1024 * 1024, '1.00 GB'],
            [1024 * 1024 * 1024 * 1024, '1.00 TB'],
        ];
    }

    /**
     * @dataProvider dataToHuman
     */
    public function testToHuman($bytes, $expected)
    {
        $actual = LibricielChecksBytesize::toHuman($bytes);
        $this->assertEquals($expected, $actual, var_export($actual, true));
    }

    public function dataFromHuman()
    {
        return [
            ['30', 30],
            ['15B', 15],
            ['66Kb', 66 * 1024],
            ['8M', 8 * 1024 * 1024],
            ['3.5G', 3.5 * 1024 * 1024 * 1024],
            ['foo', false],
        ];
    }

    /**
     * @dataProvider dataFromHuman
     */
    public function testFromHuman($human, $expected)
    {
        $actual = LibricielChecksBytesize::fromHuman($human);
        $this->assertEquals($expected, $actual, var_export($actual, true));
    }
}
