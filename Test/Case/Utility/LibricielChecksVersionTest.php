<?php

App::uses('LibricielChecksVersion', 'LibricielChecks.Utility');

/**
 * La classe LibricielChecksVersionTest est chargée des tests unitaires de la classe
 * LibricielChecksVersion du plugin LibricielChecks.
 *
 * @package LibricielChecks
 * @subpackage Test.Case
 */
class LibricielChecksVersionTest extends CakeTestCase
{
    public function dataId()
    {
        return [
            ['5', 5000000],
            ['5.6', 5060000],
            ['5.6.9', 5060900],
            ['5.6.9.1', 5060901],
            ['7.2.24-0ubuntu0.18.04.3', 7022418],
        ];
    }

    /**
     * @dataProvider dataId
     */
    public function testId($version, $expected)
    {
        $actual = LibricielChecksVersion::id($version);
        $this->assertEquals($expected, $actual, var_export($actual, true));
    }

    public function dataDifference()
    {
        return [
            ['5', '5.1', null, false],
            ['5.2', '5.1', null, true],
            ['5.2', '5.1', '5.3', true],
            ['5.2', '5.1', '5.2', false],
        ];
    }

    /**
     * @dataProvider dataDifference
     */
    public function testDifference($actual, $low, $high, $expected)
    {
        $actual = LibricielChecksVersion::difference($actual, $low, $high);
        $this->assertEquals($expected, $actual, var_export($actual, true));
    }
}
