<?php

App::uses('LibricielChecksValidation', 'LibricielChecks.Utility');

/**
 * La classe LibricielChecksValidationTest est chargée des tests unitaires de la classe
 * LibricielChecksValidation du plugin LibricielChecks.
 *
 * @package LibricielChecks
 * @subpackage Test.Case
 */
class LibricielChecksValidationTest extends CakeTestCase
{
    public function dataBytesizeComparison()
    {
        return [
            ['50M', '==', '50M', true],
            ['50M', '>=', '50M', true],
            ['50M', '>', '50M', false],
            ['50M', '<=', '50M', true],
            ['50M', '<', '50M', false],
            ['0', '>', '50M', true],
            [0, '>', '50M', true],
        ];
    }

    /**
     * Test de la méthode bytesizeComparison.
     * @dataProvider dataBytesizeComparison
     */
    public function testBytesizeComparison($check1, $operator, $check2, $expected)
    {
        $actual = LibricielChecksValidation::bytesizeComparison($check1, $operator, $check2);
        $this->assertEquals($expected, $actual, var_export($actual, true));
    }

    public function dataUrl()
    {
        return [
            // 1. Tests de la version 2.10.20 de CakePHP
            ['http://www.cakephp.org', true],
            ['https://cakephp.org', true],
            ['http://www.cakephp.org/somewhere#anchor', true],
            ['http://192.168.0.1', true],
            ['https://www.cakephp.org', true],
            ['https://cakephp.org', true],
            ['https://www.cakephp.org/somewhere#anchor', true],
            ['https://192.168.0.1', true],
            ['ftps://www.cakephp.org/pub/cake', true],
            ['ftps://cakephp.org/pub/cake', true],
            ['ftps://192.168.0.1/pub/cake', true],
            ['ftp://www.cakephp.org/pub/cake', true],
            ['ftp://cakephp.org/pub/cake', true],
            ['ftp://192.168.0.1/pub/cake', true],
            ['sftp://192.168.0.1/pub/cake', true],
            ['https://my.domain.com/gizmo/app?class=MySip;proc=start', true],
            ['www.domain.tld', true],
            ['http://123456789112345678921234567893123456789412345678951234567896123.com', true],
            ['http://www.domain.com/blogs/index.php?blog=6&tempskin=_rss2', true],
            ['http://www.domain.com/blogs/parenth()eses.php', true],
            ['http://www.domain.com/index.php?get=params&amp;get2=params', true],
            ['http://www.domain.com/ndex.php?get=params&amp;get2=params#anchor', true],
            ['http://www.domain.com/real%20url%20encodeing', true],
            ['http://en.wikipedia.org/wiki/Architectural_pattern_(computer_science)', true],
            ['http://www.cakephp.org', true, true],
            ['http://example.com/~userdir/', true],
            ['http://underscore_subdomain.example.org', true],
            ['http://_jabber._tcp.gmail.com', true],
            ['http://www.domain.longttldnotallowed', true],
            ['ftps://256.168.0.1/pub/cake', false],
            ['ftp://256.168.0.1/pub/cake', false],
            ['http://w_w.domain.co_m', false],
            ['http://www.domain.12com', false],
            ['http://www.-invaliddomain.tld', false],
            ['http://www.domain.-invalidtld', false],
            ['http://this-domain-is-too-loooooong-by-icann-rules-maximum-length-is-63.com', false],
            ['http://www.underscore_domain.org', false],
            ['http://_jabber._tcp.g_mail.com', false],
            ['http://en.(wikipedia).org/', false],
            ['http://www.domain.com/fakeenco%ode', false],
            ['--.example.com', false],
            ['www.cakephp.org', true, false],

            ['http://example.com/~userdir/subdir/index.html', true],
            ['http://www.zwischenraume.de', true],
            ['http://www.zwischenraume.cz', true],
            ['http://www.last.fm/music/浜崎あゆみ', true ],
            ['http://www.electrohome.ro/images/239537750-284232-215_300[1].jpg', true],
            ['http://www.eräume.foo', true],
            ['http://äüö.eräume.foo', true],

            ['http://cakephp.org:80', true],
            ['https://cakephp.org:443', true],
            ['https://cakephp.org:2000', true],
            ['https://cakephp.org:27000', true],
            ['https://cakephp.org:65000', true],

            ['[2001:0db8::1428:57ab]', true],
            ['[::1]', true],
            ['[2001:0db8::1428:57ab]:80', true],
            ['[::1]:80', true],
            ['http://[2001:0db8::1428:57ab]', true],
            ['http://[::1]', true],
            ['http://[2001:0db8::1428:57ab]:80', true],
            ['http://[::1]:80', true],

            ['[1::2::3]', false],

            // 2. Test des noms d'hôtes non qualifiés
            ['https://ejbca:8080/ejbca/ejbcaws/ejbcaws?wsdl', true],
            ['https://192.168.57.49:8443/ejbca/ejbcaws/ejbcaws?wsdl', true],
        ];
    }

    /**
     * @dataProvider dataUrl
     */
    public function testUrl($check, $expected)
    {
        $this->assertSame($expected, LibricielChecksValidation::url($check));
    }
}
