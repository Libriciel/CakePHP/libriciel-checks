<?php
    App::uses('AppHelper', 'View/Helper');
    App::uses('LibricielChecksTranslate', 'LibricielChecks.Utility');

/**
 * La classe ChecksHelper fournit des méthodes pour réprésentater les résultats
 * de vérifications de l'environnement applicatif.
 *
 * @package LibricielChecks
 * @subpackage View.Helper
 */
class LibricielChecksHelper extends AppHelper
{
    public $helpers = [ 'Html' ];

    /**
     *
     * @param array $elements
     * @param string $class
     * @return string
     */
    public function checklist(array $elements, $class = null)
    {
        $return = '';

        if (!empty($elements)) {
            $return .= '<ul class="libriciel-checks check ' . $class . '">';
            foreach ($elements as $element => $found) {
                $return .= $this->Html->tag('li', $element, [ 'class' => ( $found ? 'success text-success' : 'error text-danger' ) ]);
            }
            $return .= '</ul>';
        }

        return $return;
    }

    protected function _statusCell($success)
    {
        if ($success === true) {
            $text = $this->Html->tag('span', LibricielChecksTranslate::singular('Success'), ['class' => 'sr-only']);
            $icon = $this->Html->tag('span', '', ['class' => 'fa fa-check text-success']);

            return $this->Html->tag('td', "{$icon} {$text}", ['class' => 'col-xs-1 status success']);
        } else {
            $text = $this->Html->tag('span', LibricielChecksTranslate::singular('Error'), ['class' => 'sr-only']);
            $icon = $this->Html->tag('span', '', ['class' => 'fa fa-times text-danger']);

            return $this->Html->tag('td', "{$icon} {$text}", ['class' => 'col-xs-1 status error']);
        }
    }

    /**
     *
     * @param array $elements
     * @param string $class
     * @return string
     */
    public function table(array $elements, $class = 'values')
    {
        $rows = [];

        if (!empty($elements)) {
            foreach ($elements as $name => $result) {
                if (!is_array($result)) {
                    $th = $this->Html->tag('th', $name);
                    $rows[] = $this->Html->tag('tr', $th, ['class' => 'col-xs-12']);
                } else {
                    $success = isset($result['success']) && $result['success'] === true;

                    $value = ( !isset($result['value']) ? '' : $result['value'] );

                    $th = $this->Html->tag('th', $name, ['class' => 'col-xs-4']);
                    $tdValue = $this->Html->tag('td', is_array($value) ? var_export($value, true) : $value, [ 'class' => 'col-xs-3' ]);
                    $tdMessage = $this->Html->tag('td', ( !isset($result['message']) ? '' : $result['message'] ), [ 'class' => 'col-xs-4' ]);
                    $tdStatus = $this->Html->tag('td', isset($result['success']) && $result['success'] ? 'OK' : 'Erreur', [ 'class' => ( isset($result['success']) && $result['success'] ? 'col-xs-1 status success' : 'col-xs-1 status error' ) ]);

                    $rows[] = $this->Html->tag(
                        'tr',
                        $this->_statusCell($success)
                        . "{$th}{$tdValue}{$tdMessage}",
                        ['class' => $success === true ? 'success' : 'danger']
                    );
                }
            }
        }

        $tbody = $this->Html->tag('tbody', implode($rows));
        $table = $this->Html->tag('table', $tbody, [ 'class' => "table table-striped table-bordered table-hover table-condensed libriciel-checks checks {$class}" ]);

        return $this->Html->tag('div', $table, ['class' => 'table-responsive']);
    }

    public function accordion($title, $content, array $params = [])
    {
        $defaults = ['id' => uniqid('check-panel-'), 'level' => 1, 'parent' => uniqid('parent-panel-'), 'expanded' => null];
        $params += $defaults;

        $bodyId = $params['id'] . '-body';
        $headingId = $params['id'] . '-heading';

        // @todo: ="#accordion"
        $heading = $this->Html->tag(
            "h{$params['level']}",
            $this->Html->link(
                $title,
                "#{$bodyId}",
                [
                    'role' => 'button',
                    'data-toggle' => 'collapse',
                    'aria-expanded' => empty($params['expanded']) === false,
                    'aria-controls' => $bodyId,
                    'data-parent' => "#{$params['parent']}",
                    'style' => 'display: block;',
                ]
            ),
            ['class' => 'panel-title']
        );
        $headingDiv = $this->Html->tag(
            'div',
            $heading,
            [
                'id' => $headingId,
                'role' => 'tab',
                'class' => 'panel-heading',
            ]
        );

        $body = $this->Html->tag('div', $content, ['class' => 'panel-body']);
        $bodyDiv = $this->Html->tag(
            'div',
            $body,
            [
                'id' => $bodyId,
                'role' => 'tabpanel',
                'aria-labelledby' => $headingId,
                'class' => 'panel-collapse collapse',
            ]
        );

        return $this->Html->tag('div', $headingDiv . $bodyDiv, ['class' => 'libriciel-checks panel panel-default']);
    }

    public function accordions(array $contents, array $params = [])
    {
        $defaults = ['id' => uniqid('check-group-'), 'level' => 1];
        $params += $defaults;
        $return = '';

        if (empty($contents) === false) {
            foreach ($contents as $title => $content) {
                $return .= $this->accordion(
                    $title,
                    $content,
                    [
                        'level' => $params['level'],
                        'parent' => $params['id'],
                    ]
                );
            }
            $return = $this->Html->tag(
                'div',
                $return,
                [
                    'id' => $params['id'],
                    'class' => 'libriciel-checks panel-group',
                    'role' => 'tablist',
                    'aria-multiselectable' => 'true',
                ]
            );
        }

        return $return;
    }

    public function javascript()
    {
        $success = LibricielChecksTranslate::singular('Success');
        $error = LibricielChecksTranslate::singular('Error');

        $code = "$(document).ready(function () {
                try {
                    var success = '<span class=\"fa fa-check text-success\"><span class=\"sr-only\">{$success}</span></span> ',
                    error = '<span class=\"fa fa-times text-danger\"><span class=\"sr-only\">{$error}</span></span> ';

                // Traitement des panel-title contenant des statuts en erreur
                $('.libriciel-checks td.status.error').each(function(idxTd, td) {
                    $(td).parents('.panel').find('.panel-title:first').each(function(idxTitle, title) {
                        if ($(title).hasClass('error') === false) {
                            $(title).find('a').prepend(error);
                            $(title).addClass('error');
                            $(title).closest('.panel').addClass('panel-danger');
                        }
                    });
                });
        
                // Traitement des panel-title qui ne sont pas en erreur
                $('.libriciel-checks .panel-title:not(.error)').each(function(idxTitle, title) {
                    $(title).find('a').prepend(success);
                    $(title).closest('.panel').addClass('panel-success');
                });
        
                // Traitement des onglets
                $('.libriciel-checks td.status.error').each(function(idx, td) {
                    var id = $(td).closest('div.tab-pane').attr('id'),
                        link = $('a[href=\"#'+id+'\"]'),
                        li = $(link).closest('li');
        
                    if ($(li).hasClass('tab-error') === false) {
                        $(link).prepend(error);
        
                        $(li).addClass('tab-error');
                    }
                });
                $('ul.nav-tabs li').each(function(idx, li) {
                    if ($(li).hasClass('tab-error') === false) {
                        $(li).find('> a').prepend(success);
                        $(li).addClass('tab-success');
                    }
                });
            } catch(e) {
                    console.exception(e);
                }
            });";

        return $this->Html->scriptBlock($code);
    }
}
