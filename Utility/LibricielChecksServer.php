<?php
App::uses('LibricielChecksTranslate', 'LibricielChecks.Utility');

abstract class LibricielChecksServer
{
    public static function url($url, $timeout = 30, $cert = null, $passphrase = null)
    {
        $msgid = 'The webservice is unavailable: %s';
        $result = [
            'success' => null,
            'value' => $url,
            'message' => null,
        ];

        $handle = curl_init();

        $options = [
            CURLOPT_HEADER => false,
            CURLOPT_URL => $url,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => $timeout,
        ];
        if ($cert !== null) {
            $options[CURLOPT_SSLCERT] = $cert;
        }
        if ($passphrase !== null) {
            $options[CURLOPT_SSLCERTPASSWD] = $passphrase;
        }
        curl_setopt_array($handle, $options);

        $page = @curl_exec($handle);
        $code = curl_getinfo($handle, CURLINFO_HTTP_CODE);

        $error = curl_error($handle);
        if ($error) {
            $errno = curl_errno($handle);
            $result['success'] = false;
            $result['message'] = sprintf(LibricielChecksTranslate::singular($msgid), "{$errno}: {$error}");
        } elseif (preg_match('/^[2-3]/', $code) !== 1) {
            $result['success'] = false;
            $result['message'] = sprintf(LibricielChecksTranslate::singular($msgid), $page);
        } else {
            $result['success'] = true;
        }

        curl_close($handle);

        return $result;
    }

    /**
     * Vérifie l'accès à un WebService.
     *
     * @param string $wsdl L'URL du webservice
     * @param array $timeout La durée du timeout en secondes (par défaut: 30)
     * @param string $cert Le chemin vers le certificat le cas échéant
     * @param string $passphrase Le mot de passe le cas échéant
     * @return array
     */
    public static function webservice($wsdl, $timeout = 30, $cert = null, $passphrase = null)
    {
        $msgid = 'The webservice is unavailable: %s';
        $result = [
            'success' => true,
            'message' => null,
            'value' => $wsdl,
        ];

        $oldTimeout = ini_get('default_socket_timeout');
        ini_set('default_socket_timeout', $timeout);

        $result = static::url($wsdl, $timeout, $cert, $passphrase);

        if ($result['success'] === true) {
            // @info: si le wsdl ne répond pas, page blanche / erreur 500 !!
            try {
                $options = [
                    'connection_timeout' => $timeout,
                    'exceptions' => true,
                    'trace' => false,
                    'cache_wsdl' => WSDL_CACHE_NONE,
                    'stream_context' => stream_context_create(
                        [
                          'ssl' => [
                            'verify_peer' => false,
                            'verify_peer_name' => false,
                            'allow_self_signed' => true,
                          ],
                        ]
                    ),
                ];

                if ($cert !== null) {
                    $options['local_cert'] = $cert;
                }

                if ($passphrase !== null) {
                    $options['passphrase'] = $passphrase;
                }

                $client = @new SoapClient($wsdl, $options);
                $result['success'] = true;
            } catch (Exception $e) {
                $result['success'] = false;
                $result['message'] = sprintf(LibricielChecksTranslate::singular($msgid), trim($e->getMessage()));
            }
        }

        ini_set('default_socket_timeout', $oldTimeout);

        return $result;
    }

    /**
     * Vérifie l'accès à une machine distante.
     *
     * @see http://www.php.net/manual/fr/function.fsockopen.php#65631
     *
     * @param string $hostname
     * @param string $port
     * @param array $timeout La durée du timeout en secondes (par défaut: 30)
     * @return array
     */
    public static function socket($hostname, $port, $timeout = 30)
    {
        $msgid = 'La machine distante n\'est pas accessible: %s';

        $result = [
            'success' => null,
            'value' => "{$hostname}:{$port}",
            'message' => null,
        ];

        $handle = @fsockopen($hostname, $port, $errno, $errstr, $timeout);

        if (empty($handle) === false) {
            $result['success'] = true;
            fclose($handle);
        } else {
            $result['success'] = false;
            $result['message'] = sprintf(LibricielChecksTranslate::singular($msgid), "{$errno}: {$errstr}");
        }

        return $result;
    }
}
