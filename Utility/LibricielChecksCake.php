<?php
abstract class LibricielChecksCake
{
    /**
     * Vérification de l'utilisateur et du groupe propriétaires des fichiers
     * et dossiers du cache, ainsi que des permissions sur ces fichiers.
     *
     * @return array
     */
    public static function checkCachePermissions()
    {
        $return = [];
        $configNames = (array)Cache::configured();
        $user = posix_getpwuid(posix_geteuid());
        $group = posix_getgrgid($user['gid']);
        $uname = posix_uname();

        foreach ($configNames as $configName) {
            $config = Cache::config($configName);
            if (strtolower($config['engine']) === 'file') {
                $dir = $config['settings']['path'];
                if (strtolower($uname['sysname']) !== 'linux') {
                    $success = false;
                    $message = sprintf('Vérification du propriétaire et des permissions du dossier <var>%s</var> non prévue sur un système %s', $dir, $uname['sysname']);
                } else {
                    $files = [];
                    $return_var = 0;
                    $command = "find {$dir} -maxdepth 1 \\( -not -group {$group['name']} \\) -o \\( -not -user {$user['name']} \\) -o \\( -not -perm -u=rw -o -not -perm -g=rw \\) | sort";
                    exec($command, $files, $return_var);

                    if ($return_var == 0) {
                        $success = empty($files) === true;
                        $message = null;
                        if ($success === false) {
                            $message = "Des fichiers ou des dossiers du dossier <var>{$dir}</var> n'appartiennent pas au bon utilisateur ou groupe ou n'ont pas les bonnes permissions.<br/>";
                            $message .= "Il faudrait lancer la commande suivante pour corriger le problème: <code>sudo chown -R {$user['name']}:{$group['name']} {$dir} && sudo chmod o+rwX,g+rwX -R {$dir}</code><br/>";
                            $message .= "Les fichiers concernés sont: <br/><ul><li>" . implode("</li><li>", $files) . "</li></ul>";
                        }
                    } else {
                        $success = false;
                        $message = sprintf('Erreur lors de l\'exécution de la commande %s (%d)', $command, $return_var);
                    }
                }

                $return[$configName] = [
                    'success' => $success,
                    'value' => null,
                    'message' => $message,
                ];
            }
        }

        return $return;
    }

    /**
     * Effectue des tests d'écriture, de lecture et de suppression du cache,
     * pour toutes les configurations définies.
     *
     * @return array
     */
    public static function checkCacheUsage()
    {
        $cacheKeyBase = static::class . '_' . __FUNCTION__;
        $value = time();
        $return = [];

        $savedCacheDisable = Configure::read('Cache.disable');
        Configure::write('Cache.disable', false);
        $savedDebug = Configure::read('debug');
        Configure::write('debug', 0);

        $configNames = Cache::configured();
        if (!empty($configNames)) {
            foreach ($configNames as $configName) {
                if (!Cache::isInitialized($configName)) {
                    Cache::config($configName);
                }

                $cacheKey = "{$cacheKeyBase}_{$configName}";

                $write = Cache::write($cacheKey, $value, $configName);
                $read = ( Cache::read($cacheKey, $configName) == $value );
                $delete = Cache::delete($cacheKey, $configName);

                $success = ( $write && $read && $delete );
                $message = null;

                if (!$success) {
                    $actions = [];
                    if (!$write) {
                        $actions[] = 'écriture';
                    }
                    if (!$read) {
                        $actions[] = 'lecture';
                    }
                    if (!$delete) {
                        $actions[] = 'suppression';
                    }
                    $actions = implode(', ', $actions);
                    $message = "Problème(s) rencontré(s) pour \"{$configName}\": {$actions}";

                    $config = Cache::config($configName);
                    if ($config['engine'] == 'File') {
                        $path = preg_replace('/^' . preg_quote(APP, '/') . '/', APP_DIR . DS, $config['settings']['path']);
                        $message .= "; vérifiez les droits sur le répertoire: {$path}";
                    }
                }

                $return[$configName] = [
                    'success' => $success,
                    'value' => null,
                    'message' => $message,
                ];
            }
        }
        Configure::write('Cache.disable', $savedCacheDisable);
        Configure::write('debug', $savedDebug);

        return $return;
    }

    /**
     * Convertit un nombre de secondes en un nombre de jours, heures, minutes
     * et secondes.
     *
     * @url http://stackoverflow.com/posts/19680778/revisions
     *
     * @param int $seconds
     * @return string
     */
    protected static function _secondsToTime($seconds)
    {
        $dtF = new DateTime("@0");
        $dtT = new DateTime("@$seconds");
        $msgid = '%a days, %h hours, %i minutes and %s seconds';

        return $dtF->diff($dtT)->format(__($msgid));
    }

    /**
     * Retourne les noms des caches utilisés ainsi que leur durée.
     *
     * @return array
     */
    public static function checkCacheDurations()
    {
        $results = [];
        $debug = Configure::read('debug');
        $min = 60 * 60 * 24;

        foreach (Cache::configured() as $key) {
            $seconds = Hash::get(Cache::config($key), 'settings.duration');
            $message = static::_secondsToTime($seconds);
            $success = $debug || ( $seconds >= $min );

            if ($success === false) {
                $message .= sprintf(' au lieu de %s (%d)', static::_secondsToTime($min), $min);
            }

            $results[$key] = [
                'value' => $seconds,
                'success' => $success,
                'message' => $message,
            ];
        }

        ksort($results);

        return $results;
    }

    protected static function _sessionHandler()
    {
        $handler = Configure::read('Session.handler');
        if ($handler === null) {
            $handler = Configure::read('Session.defaults');
        }

        return $handler;
    }

    /**
     * Retourne le nombre de secondes avant l'expiration de la session (basé sur
     * la configuration du fichier app/Config/core.php).
     *
     * @return int
     */
    protected static function _readTimeout()
    {
        $handler = static::_sessionHandler();

        if ($handler == 'cake') {
            App::uses('CakeSession', 'Model/Datasource');

            return CakeSession::$sessionTime - CakeSession::$time;
        } else {
            return ini_get('session.gc_maxlifetime');
        }
    }

    /**
     * Retourne la vérification du timeout, avec en message la configuration
     * utilisée.
     *
     * @return array
     */
    public static function checkSessionTimeout()
    {
        $value = static::_readTimeout();
        $message = null;
        $handler = static::_sessionHandler();

        if ($handler == 'php') {
            $message = sprintf('session.gc_maxlifetime dans le php.ini (valeur actuelle: %d secondes)', ini_get('session.gc_maxlifetime'));
        } elseif ($handler == 'cake') {
            $message = sprintf('Session.timeout dans le app/Config/core.php (valeur actuelle: %d minutes)', Configure::read('Session.timeout'));
        }

        return [
            $handler => [
                'success' => true,
                'value' => static::_secondsToTime($value),
                'message' => $message,
            ],
        ];
    }

    /**
     * Lecture des chemins de configuration et validation (à minima, les
     * valeurs ne pourront pas être vides).
     *
     * @param array $paths Les chemins à vérifier
     * @return array
     */
    public static function checkConfigured(array $paths)
    {
        $return = [];
        foreach (Hash::normalize($paths) as $path => $rules) {
            $msgstr = null;
            $value = Configure::read($path);

            if (!empty($rules)) {
                foreach ($rules as $rule => $params) {
                    $params = (array)$params;
                    if (is_null($msgstr)) {
                        if (is_array($params['rule']) && isset($params['rule'][0]) == true) {
                            $rule = $params['rule'][0];
                            unset($params['rule'][0]);
                        } elseif (isset($params['rule']) == true) {
                            $rule = $params['rule'];
                            unset($params['rule']);
                        }
                        $msgstr = LibricielChecksValidator::validate($value, $rule, $params);
                    }
                }
            } elseif ($value === null) {
                $msgid = 'Missing ini configuration: %s';
                $msgstr = sprintf(LibricielChecksTranslate::singular($msgid), $path);
            }

            $return[$path] = [
                'success' => is_null($msgstr),
                'value' => $value,
                'message' => $msgstr,
            ];
        }

        return $return;
    }
}
