<?php
App::uses('LibricielChecksTranslate', 'LibricielChecks.Utility');
App::uses('LibricielChecksValidator', 'LibricielChecks.Utility');

abstract class LibricielChecksPhp
{
    /**
     * Vérifie la disponibilité d'extensions PHP.
     *
     * @param array $extensions Les extensions à vérifier.
     * @return array
     */
    public static function checkExtensions(array $extensions)
    {
        $msgstr = LibricielChecksTranslate::singular('Missing PHP extension %s');

        $checks = [];
        foreach ($extensions as $extension) {
            $success = extension_loaded($extension);

            $checks[$extension] = [
                'success' => $success,
                'value' => null,
                'message' => $success ? null : sprintf($msgstr, $extension),
            ];
        }

        return $checks;
    }

    /**
     * Vérifie la présence, et le cas échéant la configuration, de variables
     * dans le php.ini
     *
     * @param array $inis Les variables à vérifier, avec règles de validation éventuelles.
     * @return array
     */
    public static function checkInis(array $inis)
    {
        $return = [];
        foreach (Hash::normalize($inis) as $ini => $rules) {
            $msgstr = null;
            $value = ini_get($ini);

            if (!empty($rules)) {
                foreach ($rules as $rule => $params) {
                    $params = (array)$params;
                    if (is_null($msgstr)) {
                        if (is_array($params['rule']) && isset($params['rule'][0]) == true) {
                            $rule = $params['rule'][0];
                            unset($params['rule'][0]);
                        } elseif (isset($params['rule']) == true) {
                            $rule = $params['rule'];
                            unset($params['rule']);
                        }
                        $msgstr = LibricielChecksValidator::validate($value, $rule, $params);
                    }
                }
            } elseif (empty($value) === true) {
                $msgid = 'Missing ini configuration: %s';
                $msgstr = sprintf(LibricielChecksTranslate::singular($msgid), $ini);
            }

            $return[$ini] = [
                'success' => is_null($msgstr),
                'value' => $value,
                'message' => $msgstr,
            ];
        }

        return $return;
    }

    /**
     * Vérifie la présence d'extensions PEAR
     *
     * @param array $extensions
     * @param bool $base A true, vérifie la présence des classes PEAR et PEAR_Registry.
     * @return array
     */
    public static function checkPear($extensions = [], $base = true)
    {
        $return = [];
        $Registry = null;
        @include_once 'PEAR/Config.php';
        if (class_exists('PEAR_Config')) {
            $Config = new PEAR_Config();
            $Registry = $Config->getRegistry();
        }

        if ($base === true) {
            $msgid = '%s is not installed';
            $success = (bool)@include_once 'PEAR.php';
            $return['PEAR'] = [
                'success' => $success,
                'message' => $success ? null : sprintf(LibricielChecksTranslate::singular($msgid), 'PEAR'),
            ];

            if ($Registry !== null) {
                $installed = $Registry->listPackages();
                sort($installed);
                $installedMsgid = '%s is installed with the following extensions: %s';
            }
            $return['PEAR_Registry'] = [
                'success' => $Registry !== null,
                'message' => $Registry !== null
                    ? sprintf(LibricielChecksTranslate::singular($installedMsgid), 'PEAR_Registry', implode(', ', $installed))
                    : sprintf(LibricielChecksTranslate::singular($msgid), 'PEAR_Registry'),
            ];
        }

        $msgid = 'PEAR extension is missing: %s';
        foreach ($extensions as $extension) {
            $success = is_null($Registry) === false ? @$Registry->packageExists($extension) : false;
            $return[$extension] = [
                'success' => $success,
                'message' => $success === true
                    ? null
                    : sprintf(LibricielChecksTranslate::singular($msgid), $extension),
            ];
        }

        return $return;
    }

    /**
     * Lecture des chemins de configuration et validation (à minima, les
     * valeurs ne pourront pas être vides).
     *
     * @param array $paths Les chemins à vérifier
     * @return array
     */
    public static function checkDefined(array $paths)
    {
        $return = [];
        $defaults = [
            'allowEmpty' => false,
            'required' => true,
        ];

        foreach (Hash::normalize($paths) as $path => $rules) {
            $msgstr = null;
            $value = constant($path);
            foreach ($rules as $rule => $params) {
                $params = (array)$params;
                if (is_null($msgstr)) {
                    if (is_array($params['rule']) && isset($params['rule'][0]) == true) {
                        $rule = $params['rule'][0];
                        unset($params['rule'][0]);
                    } elseif (isset($params['rule']) == true) {
                        $rule = $params['rule'];
                        unset($params['rule']);
                    }
                    $msgstr = LibricielChecksValidator::validate($value, $rule, $params);
                }
            }
            $return[$path] = [
                'success' => $msgstr === null,
                'value' => $value,
                'message' => $msgstr,
            ];
        }

        ksort($return);

        return $return;
    }
}
