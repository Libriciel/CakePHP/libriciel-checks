<?php
// @info http://php.net/manual/fr/function.clearstatcache.php

App::uses('LibricielChecksBytesize', 'LibricielChecks.Utility');
App::uses('LibricielChecksTranslate', 'LibricielChecks.Utility');

abstract class LibricielChecksFilesystem
{
    protected static function _return()
    {
        return [
            'success' => null,
            'value' => null,
            'message' => null,
        ];
    }

    public static function relpath($path, $root = null)
    {
        if ($root === null) {
            $root = ROOT . DS;
        }

        return preg_replace('/^' . preg_quote($root, '/') . '/', '', $path);
    }

    protected static function _expectedPermissions($permissions)
    {
        $result = [
            'r' => mb_strstr($permissions, 'r') !== false,
            'w' => mb_strstr($permissions, 'w') !== false,
            'x' => mb_strstr($permissions, 'x') !== false,
        ];
        // @todo: error

        return $result;
    }

    protected static function _actualPermissions($path)
    {
        $result = [
            'r' => is_readable($path),
            'w' => is_writable($path),
            'x' => is_executable($path),
        ];

        return $result;
    }

    /**
     *
     * @param type $path
     * @param string $permissions rwx
     * @param type $root
     * @return type
     */
    public static function checkPermissions($path, $permissions = 'r', $root = null)
    {
        if (is_array($path) === true) {
            $return = [];
            foreach (Hash::normalize($path) as $value => $perm) {
                $return[static::relpath($value, $root)] = static::checkPermissions(
                    $value,
                    $perm ? $perm : $permissions,
                    $root
                );
            }

            return $return;
        }

        $return = static::_return();
        $expected = array_keys(array_filter(static::_expectedPermissions($permissions)));
        $actual = static::_actualPermissions($path);
        $relpath = static::relpath($path, $root);

        $missing = [];
        foreach ($expected as $key) {
            if ($actual[$key] !== true) {
                $missing[] = $key;
            }
        }

        $return['success'] = empty($missing);
        $return['value'] = implode('', $expected);

        if (empty($missing) !== true) {
            $msgstr = LibricielChecksTranslate::plural(
                'Missing permission %s for path %s',
                'Missing permissions %s for path %s',
                count($missing)
            );
            $return['message'] = sprintf($msgstr, implode('', $missing), $relpath);
        }

        return $return;
    }

    /**
     * Vérifie l'espace disponible (par-rapport au seuil) pour tous les
     * répertoires passés en paramètre.
     *
     * @param string|array $directories Le ou les répertoires à vé"rifier.
     *  Il est possible de passer un array avec le chemin en clé et un array
     *  en valeur, contenant la clé threshold pour ce répertoire en particulier.
     * @param array $params La clé threshold permet de spécifier le nombre
     *   d'octets minimum requis (l'équivalent de 100 Mo par défaut).
     * @return array
     */
    public static function checkFreeSpace($directories, array $params = [])
    {
        $directories = Hash::normalize((array)$directories);
        $params += ['threshold' => 104857600];
        $return = [];

        foreach ($directories as $directory => $options) {
            $relpath = static::relpath($directory);
            $return[$relpath] = [
                'success' => true,
                'value' => null,
                'message' => null,
            ];
            $options = (array)$options;
            $options += $params;

            if (is_dir($directory) === false) {
                $return[$relpath]['success'] = false;
                // @todo: LibricielChecksTranslate::plural
                $msgstr = 'Le répertoire %s n\'existe pas';
                $return[$relpath]['message'] = sprintf($msgstr, $relpath);
            } else {
                $freeBytes = disk_free_space($directory);
                $return[$relpath]['value'] = LibricielChecksBytesize::toHuman($freeBytes);

                if ($freeBytes < $options['threshold']) {
                    $return[$relpath]['success'] = false;
                    $msgstr = 'L\'espace disponible dans le répertoire %s devrait être d\'au moins %s mais il ne reste actuellement que %s';
                    $return[$relpath]['message'] = sprintf(
                        $msgstr,
                        $relpath,
                        LibricielChecksBytesize::toHuman($options['threshold']),
                        LibricielChecksBytesize::toHuman($freeBytes)
                    );
                }
            }
        }

        return $return;
    }

    public static function checkBinaries($paths)
    {
        // @todo: LibricielChecksTranslate::singular
        $msgstr = 'Le binaire %s n\'est pas accessible sur le système.';
        $return = [];

        foreach ($paths as $path) {
            $which = exec("which {$path}");
            $success = !empty($which);

            if ($success === false) {
                $return[$path] = [
                    'success' => $success,
                    'value' => $path,
                    'message' => sprintf($msgstr, $path),
                ];
            } else {
                $return[$path] = static::checkPermissions($path, 'rx');
            }
        }

        return $return;
    }
}
