<?php

App::uses('LibricielChecksTranslate', 'LibricielChecks.Utility');

abstract class LibricielChecksApache
{
    /**
     * Vérifie la disponibilité de modules Apache
     *
     * @param array $modules Les modules à vérifier.
     * @param string $msgid Le gabarit du message à utiliser en cas de non disponibilité.
     * @return array
     */
    public static function modules(array $modules, $msgid = 'Missing Apache extension %s')
    {
        $msgstr = LibricielChecksTranslate::singular($msgid);

        $loaded = strpos(php_sapi_name(), 'apache') !== false
            ? apache_get_modules()
            : [];

        $checks = [];
        foreach ($modules as $module) {
            $success = in_array($module, $loaded);

            $checks[$module] = [
                'success' => $success,
                'message' => ( $success ? null : sprintf($msgstr, $module) ),
            ];
        }

        return $checks;
    }
}
