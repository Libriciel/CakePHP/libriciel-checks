<?php

/**
 * Code source de la classe LibricielChecksValidation.
 *
 * @package LibricielChecks
 * @subpackage Lib
 * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
 */
App::uses('LibricielChecksBytesize', 'LibricielChecks.Utility');
App::uses('Validation', 'Utility');

/**
 * La classe LibricielChecksValidation fournit des méthodes de validation de paramètres
 * simples.
 *
 * Cette classe est une adaptation d'une classe du coeur de 1.2.11.
 *
 * phpcs:disable PSR1.Methods.CamelCapsMethodName.NotCamelCaps
 *
 * @package LibricielChecks
 * @subpackage Lib
 */
class LibricielChecksValidation
{
    /**
     * Some complex patterns needed in multiple places
     * Vient du coeur de CakePHP 1.2.11
     *
     * @var array
     * @access private
     */
    protected $__pattern = [
        'ip' => '(?:(?:25[0-5]|2[0-4][0-9]|(?:(?:1[0-9])?|[1-9]?)[0-9])\.){3}(?:25[0-5]|2[0-4][0-9]|(?:(?:1[0-9])?|[1-9]?)[0-9])',
        'hostname' => '(?:[a-z0-9][-a-z0-9]*\.)*(?:[a-z0-9][-a-z0-9]{0,62})\.(?:(?:[a-z]{2}\.)?[a-z]{2,4}|museum|travel)',
    ];

    /**
     *
     * @staticvar array $instance
     * @return \LibricielChecksValidation
     */
    public static function &getInstance()
    {
        static $instance = [];

        if (!$instance) {
            $instance[0] = new static();
        }

        return $instance[0];
    }

    /**
     *
     * @param type $check
     * @return bool
     */
    public function integer($check)
    {
        return is_int($check);
    }

    /**
     *
     * @param type $check
     * @return bool
     */
    public function numeric($check)
    {
        return is_numeric($check);
    }

    /**
     *
     * @param type $check
     * @param type $list
     * @return type
     */
    public function inList($check, $list)
    {
        return in_array($check, $list);
    }

    /**
     *
     * @param type $check
     * @return type
     */
    public function string($check)
    {
        return is_string($check);
    }

    /**
     *
     * @param type $check
     * @return type
     */
    public function boolean($check)
    {
        return is_bool($check);
    }

    /**
     *
     * @param type $check
     * @return type
     */
    public function isarray($check)
    {
        return is_array($check);
    }

    /**
     * Vérifie que la ou les valeurs fassent toutes partie de la liste des
     * valeurs acceptées.
     *
     * @param mixed $check Une valeur ou un ensemble de valeurs
     * @param array $accepted La liste des valeurs acceptées
     * @return bool
     */
    public function inListArray($check, array $accepted)
    {
        $check = (array)$check;

        foreach ($check as $value) {
            if (in_array($value, $accepted) === false) {
                return false;
            }
        }

        return true;
    }

    /**
     *
     * @param type $check
     * @return type
     */
    public function dir($check)
    {
        return is_dir($check) && is_readable($check);
    }

    /**
     *
     * @param type $check
     * @return type
     */
    public function writableDir($check)
    {
        return is_dir($check) && is_writable($check);
    }

    /**
     * Valide un pattern d'expression régulière.
     *
     * @param string $check
     * @return bool
     */
    public function preg_pattern($check)
    {
        return @preg_match($check, '') !== false;
    }

    /**
     * Effectue Validation::comparison après avoir transformé les quantités au moyen de la fonction
     * LibricielChecksBytesize::fromHuman.
     * Si une quantité vaut 0, elle sera considérée comme infinie.
     *
     * @param string $check1
     * @param null|string $operator
     * @param null|string $check2
     * @return bool
     */
    public function bytesizeComparison($check1, $operator = null, $check2 = null)
    {
        if (is_numeric($check1) === true && (string)$check1 === '0') {
            $check1 = PHP_INT_MAX;
        }

        if (is_numeric($check2) === true && (string)$check2 === '0') {
            $check2 = PHP_INT_MAX;
        }

        return Validation::comparison(
            LibricielChecksBytesize::fromHuman($check1),
            $operator,
            $check2 === null ? null : LibricielChecksBytesize::fromHuman($check2)
        );
    }

    /**
     * Surcharge de la méthode url pour permettre les noms d'hôtes non qualifiés (ex.: https://ejbca:8080/).
     *
     * @param string $check Value to check
     * @param bool $strict Require URL to be prefixed by a valid scheme (one of http(s)/ftp(s)/file/news/gopher)
     * @return bool Success
     */
    public static function url($check, $strict = false)
    {
        $regexp = '/^(?<scheme>(https?|ftps?|sftp|file|news|gopher):\/\/){0,1}'
            . '(?<main>[^\/:]+)'
            . '(?<port>:[0-9]+)?'
            . '(?<end>\/.*)?$/u';
        if (preg_match($regexp, $check, $matches) === 1) {
            $matches = array_filter($matches, 'is_string', ARRAY_FILTER_USE_KEY) + ['port' => '', 'end' => ''];
            $matches['main'] = preg_replace('/^([^\.]+)$/u', '\1.com', $matches['main']);
            $check = implode('', $matches);
        }

        return Validation::url($check, $strict);
    }
}
