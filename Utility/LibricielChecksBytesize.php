<?php

App::uses('LibricielChecksTranslate', 'LibricielChecks.Utility');

abstract class LibricielChecksBytesize
{
    /**
     * Retourne une représentation lisible par un être humain à partir d'un
     * nombre d'octets.
     *
     * @see http://www.phpfront.com/php/Convert-Bytes-to-corresponding-size/
     *
     * @param int $bytes
     * @return string
     */
    public static function toHuman($bytes)
    {
        $size = $bytes / 1024;
        $format = '%d B';

        if ($size < 1024) {
            $size = number_format($size, 2);
            $format = '%.2f KB';
        } else {
            if ($size / 1024 < 1024) {
                $size = number_format($size / 1024, 2);
                $format = '%.2f MB';
            } elseif ($size / 1024 / 1024 < 1024) {
                $size = number_format($size / 1024 / 1024, 2);
                $format = '%.2f GB';
            } elseif ($size / 1024 / 1024 / 1024 < 1024) {
                $size = number_format($size / 1024 / 1024 / 1024, 2);
                $format = '%.2f TB';
            }
        }

        return sprintf(LibricielChecksTranslate::singular($format), $size);
    }

    /**
     * Convertit une chaîne de caractères représentant une taille de fichiers en un nombre d'octets.
     *
     * @param string $human
     * @return int|false
     */
    public static function fromHuman($human)
    {
        $result = false;

        $matches = [];
        $regexp = '/^(?P<amount>[0-9]+(\.[0-9]+)?)(?P<unit>[KMGT]{0,1})B{0,1}$/i';
        if (preg_match($regexp, $human, $matches)) {
            switch (mb_strtoupper($matches['unit'])) {
                case 'K':
                    $result = $matches['amount'] * 1024;
                    break;
                case 'M':
                    $result = $matches['amount'] * 1024 * 1024;
                    break;
                case 'G':
                    $result = $matches['amount'] * 1024 * 1024 * 1024;
                    break;
                case 'T':
                    $result = $matches['amount'] * 1024 * 1024 * 1024 * 1024;
                    break;
                default:
                    $result = $matches['amount'];
            }
        }

        return $result;
    }
}
