<?php
abstract class LibricielChecksTranslate
{
    protected static $_singular = '__';

    protected static $_plural = '__n';

    protected static $_domain = null;

    protected static $_initialized = false;

    protected static function _init()
    {
        if (static::$_initialized === false) {
            // @todo: Configure::read('LibricielChecks.LibricielChecksTranslate')
            static::$_initialized = true;
        }
    }

    public static function singular($singular, $domain = null)
    {
        static::_init();

        if ($domain === null) {
            $domain = static::$_domain;
        }

        if ($domain === null) {
            $msgstr = call_user_func_array(static::$_singular, [$singular]);
        } else {
            $msgstr = call_user_func_array(static::$_singular, [$domain, $singular]);
        }

        return $msgstr;
    }

    public static function plural($singular, $plural, $count, $domain = null)
    {
        static::_init();

        if ($domain === null) {
            $domain = static::$_domain;
        }

        if ($domain === null) {
            $msgstr = call_user_func_array(static::$_plural, [$singular, $plural, $count]);
        } else {
            $msgstr = call_user_func_array(static::$_plural, [$domain, $singular, $plural, $count]);
        }

        return $msgstr;
    }
}
