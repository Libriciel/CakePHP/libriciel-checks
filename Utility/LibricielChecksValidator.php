<?php

App::uses('LibricielChecksTranslate', 'LibricielChecks.Utility');
App::uses('LibricielChecksValidation', 'LibricielChecks.Utility');
App::uses('LibricielChecksVersion', 'LibricielChecks.Utility');
App::uses('Validation', 'Utility');

abstract class LibricielChecksValidator
{
    /**
     * Vérifie qu'un logiciel sse trouve dans une version donnée.
     *
     * @param string $software Le nom du logiciel
     * @param string $actual La version du logiciel
     * @param string $low La version minimale
     * @param string $high La version maximale
     * @return array
     */
    public static function version($software, $actual, $low, $high = null)
    {
        $success = LibricielChecksVersion::difference($actual, $low, $high);
        $message = null;

        if ($success !== true) {
            if (is_null($high) === true) {
                $msgid = 'The version of %s should be higher or equal to %s';
                $message = sprintf(LibricielChecksTranslate::singular($msgid), $software, $low);
            } else {
                $msgid = 'The version of %s should be higher or equal to %s and also be lower than %s';
                $message = sprintf(LibricielChecksTranslate::singular($msgid), $software, $low, $high);
            }
        }

        return [
            'value' => $actual,
            'success' => $success,
            'message' => $message,
        ];
    }

    /**
     * Validation d'une valeur par-rapport à une règle de validation.
     * Retourne un message d'erreur en cas de non validation, null sinon.
     *
     * @param mixed $value La valeur à tester.
     * @param string $rule Le nom de la règle à vérifier.
     * @param array $ruleParams Les paramètres à passer à la règle.
     * @return string
     */
    public static function validate($value, $rule, $ruleParams = [])
    {
        $ruleParams += ['allowEmpty' => false, 'message' => null];
        $allowEmpty = $ruleParams['allowEmpty'];
        $message = $ruleParams['message'];
        $return = null;
        unset($ruleParams['allowEmpty'], $ruleParams['message']);

        if (strpos($rule, '::') !== false) {
            list($validationClass, $validationMethod) = explode('::', $rule);
        } else {
            $validationClass = null;
            $validationMethod = $rule;
        }

        if ($validationClass !== null) { // @todo: plugin
            App::uses($validationClass, 'Utility');
        } elseif (method_exists('LibricielChecksValidation', $validationMethod) === true) {
            $validationClass = 'LibricielChecksValidation';
        } elseif (method_exists('Validation', $validationMethod) === true) {
            $validationClass = 'Validation';
        }

        if ($validationClass !== null) {
            $testValue = $value;

            if (isset($ruleParams['rule'])) {
                $ruleParams = $ruleParams['rule'];
            }

            if (method_exists('LibricielChecksValidation', $rule) === true) {
                $validator = LibricielChecksValidation::getInstance();
                $validate = call_user_func_array([$validator, $rule], array_merge([$testValue], $ruleParams));
            } elseif (method_exists($validationClass, $validationMethod)) {
                $validator = $validationClass;
                $testRuleParams = $ruleParams;

                if (isset($testRuleParams[0]) && $testRuleParams[0] == $rule) {
                    $testRuleParams[0] = $testValue;
                } else {
                    array_unshift($testRuleParams, $testValue);
                }

                $validate = call_user_func_array([$validator, $validationMethod], $testRuleParams);
            } else {
                $msgid = 'Validation method %s::%s does not exist';
                throw new RuntimeException(sprintf($msgid, $validationClass, $validationMethod));
            }

            if (!( $allowEmpty && empty($value) ) && ( is_null($value) || !$validate )) {
                $message = $message ?? "Validate::{$validationMethod}";
                if (isset($ruleParams[0]) && $ruleParams[0] == $rule) {
                    unset($ruleParams[0]);
                }
                $msgstr = __d('database', $message);
                if ($msgstr === $message) {
                    $msgstr = __($message);
                }
                $sprintfParams = Hash::merge([$msgstr], $ruleParams);
                $count = count($sprintfParams);
                for ($i = 1; ( $i <= $count - 1); $i++) {
                    if (is_array($sprintfParams[$i])) {
                        $sprintfParams[$i] = implode(', ', $sprintfParams[$i]);
                    }
                }
                $return = call_user_func_array('sprintf', $sprintfParams);
            }
        } else {
            $return = "La méthode de validation {$rule} n'existe pas.";
        }

        return $return;
    }
}
