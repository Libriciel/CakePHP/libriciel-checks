<?php

abstract class LibricielChecksVersion
{
    /**
     * Retourne un nombre qui sera plus grand pour une version plus élevée.
     *
     * @see http://az.php.net/manual/en/function.phpversion.php (Exemple 2: PHP_VERSION_ID)
     *
     * @param string $version La version pour laquelle obtenir l'id
     * @return int
     */
    public static function id($version)
    {
        $tokens = explode('.', $version);
        $multipliers = [ 0 => 1000000, 1 => 10000, 2 => 100, 3 => 1 ];

        $result = 0;
        for ($i = 0; $i <= 3; $i++) {
            if (isset($tokens[$i])) {
                $number = preg_replace('/^([0-9]+)\-.*$/', '\1', $tokens[$i]);
                $result += $multipliers[$i] * $number;
            }
        }

        return $result;
    }

    /**
     * Vérifie qu'une version donnée soit au minimum égale à une certaine version
     * et soit éventuellement plus petite qu'une version maximale.
     *
     * @param string $actual La version à tester
     * @param string $low La version minimale
     * @param string $high La version maximale éventuelle
     * @return bool
     */
    public static function difference($actual, $low, $high = null)
    {
        $actual = static::id($actual);
        $low = static::id($low);
        $high = ( is_null($high) ? null : static::id($high) );

        $success = ( $actual >= $low );

        if ($success === true && is_null($high) === false) {
            $success = ( $actual < $high );
        }

        return $success;
    }
}
