<?php

App::uses('CakeEmail', 'Network/Email');
App::uses('LibricielChecksServer', 'LibricielChecks.Utility');
App::uses('LibricielChecksTranslate', 'LibricielChecks.Utility');

abstract class LibricielChecksCakeEmail
{
    /**
     * Vérification d'une configuration de CakeEmail dans le fichier
     * app/Config/email.php.
     *
     * EmailComponent est déprécié depuis la version 2.0 de CakePHP.
     *
     * @todo ::transportClass
     *
     * @param string $name Le nom de la configuration
     * @return array
     */
    public function checkConfig($name)
    {
        $settingsFile = 'app/Config/email.php';
        $settingsMailFields = ['from', 'replyTo', 'readReceipt', 'returnPath', 'sender', 'to', 'cc', 'bcc'];

        $configure = [];
        $tests = [];

        try {
            $Email = new CakeEmail($name);
            $settings = $Email->config();
        } catch (Exception $e) {
            $Email = null;
            $settings = [];
        }

        if (empty($settings)) {
            $configure["Présence de la clé {$name} dans le fichier {$settingsFile}"] = [
                'success' => false,
                'message' => "Configuration manquante ou erronée dans le fichier {$settingsFile}",
            ];
        } else {
            // La configuration est-elle présente ?
            $configure[$settingsFile] = [
                'success' => true,
                'message' => null,
            ];

            // Vérification des adresses mail
            foreach ($settingsMailFields as $emailAddress) {
                if (isset($settings[$emailAddress]) && !empty($settings[$emailAddress])) {
                    foreach ((array)$settings[$emailAddress] as $key => $value) {
                        if (is_numeric($key) === true) {
                            $address = $value;
                        } else {
                            $address = $key;
                        }
                        $success = Validation::email($address, true);

                        $configure["{$emailAddress}.{$address}"] = [
                            'success' => $success,
                            'message' => ( $success ? null : "L'adresse mail {$address} n'est pas valide" ),
                        ];
                    }
                }
            }

            // Test de connexion au serveur SMTP
            if (!is_null($Email) && ( isset($settings['transport']) && strtolower($settings['transport']) == 'smtp' )) {
                $tests['Connexion au serveur'] = LibricielChecksServer::socket($settings['host'], $settings['port']);
            }

            // Test d'envoi de mail factice
            $success = false;
            if (!is_null($Email)) {
                try {
                    $Email->subject('Test');
                    $Email->to($Email->from());
                    $Email->transport('Debug');
                    $result = $Email->send('Test');
                    $success = !empty($result);
                } catch (Exception $e) {
                    $success = false;
                }
            }
            $tests['Envoi factice'] = [
                'success' => $success,
                'message' => (!empty($success) ? null : 'Erreur lors du test d\'envoi de mail factice' ),
            ];
        }

        return ['configure' => $configure, 'tests' => $tests];
    }
}
