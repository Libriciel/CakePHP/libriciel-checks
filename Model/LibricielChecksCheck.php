<?php
/**
 * Code source de la classe LibricielChecksCheck.
 *
 * @package LibricielChecks
 * @subpackage Model
 * @license CeCiLL V2 (http://www.cecill.info/licences/Licence_CeCILL_V2-fr.html)
 */
App::uses('AppModel', 'Model');
App::uses('CakeEmail', 'Network/Email');
App::uses('ChecksValidation', 'LibricielChecks.Utility');
App::uses('LibricielChecksBytesize', 'LibricielChecks.Utility');
App::uses('LibricielChecksVersion', 'LibricielChecks.Utility');
App::uses('Validation', 'Utility');

/**
 * La classe Check fournit des méthodes de vérification de l'environnement
 * applicatif.
 *
 * @deprecated since 1.2.2
 *
 * @package LibricielChecks
 * @subpackage Model
 */
class LibricielChecksCheck extends AppModel
{
    /**
     * @var string
     */
    public $name = 'Check';

    /**
     * @var string
     */
    public $useTable = false;

    /**
     * Fonction la liste des clés de l'objet Configure.
     * A utiliser lors du développement.
     *
     * @deprecated
     *
     * @return array
     */
    public function configureKeys()
    {
        $Configure = Configure::getInstance();
        $Configure = Hash::flatten($Configure);

        return array_keys($Configure);
    }

    /**
     * Vérifie la disponibilité de modules Apache
     *
     * @deprecated
     *
     * @param array $modules Les modules à vérifier.
     * @param string $message Le gabarit du message à utiliser en cas de non disponibilité.
     * @return array
     */
    public function apacheModules(array $modules, $message = "Le module Apache %s n'est pas disponible.")
    {
        $loaded = apache_modules();

        $checks = [];
        foreach ($modules as $module) {
            $success = in_array($module, $loaded);

            $checks[$module] = [
                'success' => $success,
                'message' => ( $success ? null : sprintf($message, $module) ),
            ];
        }

        return $checks;
    }

    /**
     * Vérifie la disponibilité d'extensions PHP.
     *
     * @deprecated
     *
     * @param array $extensions Les extensions à vérifier.
     * @param string $message Le gabarit du message à utiliser en cas de non disponibilité.
     * @return array
     */
    public function phpExtensions(array $extensions, $message = "L'extension PHP %s n'est pas disponible.")
    {
        $checks = [];
        foreach ($extensions as $extension) {
            $success = extension_loaded($extension);

            $checks[$extension] = [
                'success' => $success,
                'message' => ( $success ? null : sprintf($message, $extension) ),
            ];
        }

        return $checks;
    }

    /**
     * Vérifie la configuration de variables dans le php.ini
     *
     * @deprecated
     *
     * @param array $inis Les variables à vérifier, avec règles de validation éventuelles.
     * @param string $message Le gabarit du message à utiliser en cas de non disponibilité.
     * @return array
     */
    public function phpInis(array $inis, $message = "Le paramétrage de %s doit être fait dans le fichier php.ini")
    {
        $checks = [];
        foreach (Hash::normalize($inis) as $ini => $rules) {
            $message = null;
            $value = ini_get($ini);

            if (!empty($rules)) {
                foreach ($rules as $rule => $ruleParams) {
                    if (is_null($message)) {
                        $message = $this->_validate($value, $rule, $ruleParams);
                    }
                }
            } else {
                $message = ( !empty($value) ? null : sprintf($message, $ini) );
            }

            $checks[$ini] = [
                'value' => $value,
                'success' => is_null($message),
                'message' => $message,
            ];
        }

        return $checks;
    }

    /**
     * Vérifie la présence de binaires sur le système.
     *
     * @deprecated
     *
     * @param array $binaries Les binaires à vérifier.
     * @param string $message Le gabarit du message à utiliser en cas d'absence.
     * @return array
     */
    public function binaries(array $binaries, $message = "Le binaire %s n'est pas accessible sur la système.")
    {
        $checks = [];
        foreach ($binaries as $binary) {
            $which = exec("which {$binary}");
            $success = !empty($which);

            $checks[$binary] = [
                'success' => $success,
                'message' => ( $success ? null : sprintf($message, $binary) ),
            ];
        }

        return $checks;
    }

    /**
     * Vérifie si des fichiers sont présents et accessibles en lecture.
     *
     * @deprecated
     *
     * @param array $files Les chemins vers les fichiers à vérifier
     * @param string $base Le répertoire de base de l'application (en général ROOT.DS)
     * @return array
     */
    public function files(array $files, $base = null)
    {
        if (!is_null($base)) {
            $base = '/^' . preg_quote($base, '/') . '/';
        }
        $checks = [];
        foreach ($files as $file) {
            if (is_null($base)) {
                $key = $file;
            } else {
                $key = preg_replace($base, '', $file);
            }
            $checks[$key] = $this->filePermission($file, 'r');
        }

        return $checks;
    }

    /**
     * Vérifie les permissions sur un répertoire.
     *
     * @deprecated
     *
     * @todo is_executable
     *
     * @param string $directory
     * @param string $permission
     * @return array
     */
    public function directoryPermission($directory, $permission = 'r')
    {
        if (!in_array($permission, [ 'r', 'w' ])) {
            trigger_error(sprintf(__('Paramètre non permis dans %s::%s: %s. Paramètres permis: \'r\' ou \'w\''), self::class, __FUNCTION__, $permission), E_USER_WARNING);
        }

        $success = true;
        $message = null;
        if (!is_dir($directory)) {
            $success = false;
            $message = "Le dossier {$directory} n'existe pas.";
        } elseif (!is_readable($directory)) {
            $success = false;
            $message = "Le dossier {$directory} n'est pas lisible.";
        } elseif ($permission == 'w' && !is_writable($directory)) {
            $success = false;
            $message = "Le dossier {$directory} n'est pas inscriptible.";
        }

        return [
            'success' => $success,
            'message' => $message,
        ];
    }

    /**
     * Vérifie les permissions sur des répertoires.
     *
     * @deprecated
     *
     * @param array $directories
     * @param string $base Le répertoire de base de l'application (en général ROOT.DS)
     * @return array
     */
    public function directories(array $directories, $base = null)
    {
        if (!is_null($base)) {
            $base = '/^' . preg_quote($base, '/') . '/'; // FIXME
        }
        $checks = [];
        foreach (Hash::normalize($directories) as $directory => $mode) {
            if (is_null($base)) {
                $key = $directory;
            } else {
                $key = preg_replace($base, '', $directory);
            }
            $checks[$key] = $this->directoryPermission($directory, $mode);
        }

        return $checks;
    }

    /**
     * Vérifie les paermissions sur un fichier.
     *
     * @deprecated
     *
     * @todo is_executable
     *
     * @param string $file
     * @param string $permission
     * @return array
     */
    public function filePermission($file, $permission = 'r')
    {
        if (!in_array($permission, [ 'r', 'w' ])) {
            trigger_error(sprintf(__('Paramètre non permis dans %s::%s: %s. Paramètres permis: \'r\' ou \'w\''), self::class, __FUNCTION__, $permission), E_USER_WARNING);
        }

        $success = true;
        $message = null;
        if (!file_exists($file)) {
            $success = false;
            $message = "Le fichier {$file} n'existe pas.";
        } elseif (!is_readable($file)) {
            $success = false;
            $message = "Le fichier {$file} n'est pas lisible.";
        } elseif ($permission == 'w' && !is_writable($file)) {
            $success = false;
            $message = "Le fichier {$file} n'est pas inscriptible.";
        }

        return [
            'success' => $success,
            'value' => "{$file} ({$permission})",
            'message' => $message,
        ];
    }

    /**
     * Vérifie la présence de modèles odt.
     *
     * @todo utiliser files avec un préfixe
     *
     * @param array $modeles
     * @param string $prefixPath Le répertoire de base des modèles.
     * @return array
     */
    public function modelesOdt(array $modeles, $prefixPath)
    {
        $return = [];
        foreach ($modeles as $modele) {
            $return[$modele] = $this->filePermission($prefixPath . $modele, 'r');
        }
        ksort($return);

        return $return;
    }

    /**
     * Validation d'une valeur par-rapport à une règle de validation.
     * Retourne un message d'erreur en cas de non validation, null sinon.
     *
     * @deprecated
     *
     * @param mixed $value La valeur à tester.
     * @param string $rule Le nom de la règle à vérifier.
     * @param array $ruleParams Les paramètres à passer à la règle.
     * @return string
     */
    protected function _validate($value, $rule, $ruleParams = [])
    {
        $allowEmpty = $ruleParams['allowEmpty'];
        unset($ruleParams['allowEmpty']);
        $message = null;

        if (method_exists('ChecksValidation', $rule) || method_exists('Validation', $rule)) {
            // FIXME: nettoyage des URL contenant %s (pour le CG 58) et des espaces
            $testValue = $value;
            if ($rule == 'url') {
                if (stripos($testValue, '%s') !== false) {
                    $testValue = str_replace('%s', 'XXXX', $testValue);
                }
                if (stripos($testValue, ' ') !== false) {
                    $testValue = str_replace(' ', '%20', $testValue);
                }
                if (stripos($testValue, 'http://localhost/') !== false) {
                    $testValue = str_replace('http://localhost/', 'http://127.0.0.1/', $testValue);
                }
            }

            if (isset($ruleParams['rule'])) {
                $ruleParams = $ruleParams['rule'];
            }

            if (method_exists('Validation', $rule)) {
                $Validator = 'Validation';
                $testRuleParams = $ruleParams;

                if (isset($testRuleParams[0]) && $testRuleParams[0] == $rule) {
                    $testRuleParams[0] = $testValue;
                } else {
                    array_unshift($testRuleParams, $testValue);
                }

                $validate = call_user_func_array([ $Validator, $rule ], $testRuleParams);
            } elseif (method_exists('ChecksValidation', $rule)) {
                $Validator = ChecksValidation::getInstance();
                $validate = call_user_func_array([ $Validator, $rule ], array_merge([ $testValue ], $ruleParams));
            }

            if (!( $allowEmpty && empty($value) ) && ( is_null($value) || !$validate )) {
                $message = "Validate::{$rule}";
                if (isset($ruleParams[0]) && $ruleParams[0] == $rule) {
                    unset($ruleParams[0]);
                }
                $sprintfParams = Hash::merge([ __d('database', $message) ], $ruleParams);
                $count = count($sprintfParams);
                for ($i = 1; ( $i <= $count - 1 ); $i++) {
                    if (is_array($sprintfParams[$i])) {
                        $sprintfParams[$i] = implode(', ', $sprintfParams[$i]);
                    }
                }
                $message = call_user_func_array('sprintf', $sprintfParams);
            }
        } else {
            $message = "La méthode de validation {$rule} n'existe pas.";
        }

        return $message;
    }

    /**
     * Valide la valeur donnée par un chemin de configuration par une règle
     * de validation.
     *
     * @deprecated
     *
     * @param string $path Le chemin de la configuration.
     * @param string $rule Le nom de la règle à vérifier.
     * @param array $ruleParams Les paramètres à passer à la règle.
     * @return array
     */
    public function validateConfigurePath($path, $rule, $ruleParams = [])
    {
        $value = Configure::read($path);

        $message = $this->_validate($value, $rule, $ruleParams);

        return [
            'success' => is_null($message),
            'value' => var_export($value, true),
            'message' => $message,
        ];
    }

    /**
     * Lecture des chemins de configuration et validation (à minima, les
     * valeurs ne pourront pas être vides).
     *
     * @deprecated
     *
     * @param array $paths Les chemins à vérifier
     * @return array
     */
    public function configure(array $paths)
    {
        $return = [];
        $defaults = [
            'allowEmpty' => false,
//              'required' => true,
        ];

        foreach (Hash::normalize($paths) as $path => $rules) {
            if (!is_array($rules)) {
                $rules = [ [ 'rule' => $rules ] ];
            }

            foreach ($rules as $rule) {
                $rule = Hash::merge($defaults, $rule);
                if (!isset($return[$path]) || empty($return[$path]) || $return[$path]['success']) {
                    $ruleParams = $rule;
                    unset($ruleParams['rule']);
                    $validate = $this->validateConfigurePath($path, $rule['rule'], $ruleParams);
                    $return[$path] = $validate;
                }
            }
        }
        ksort($return);

        return $return;
    }

    /**
     * Valide la valeur d'une constante par une règle de validation.
     *
     * @deprecated
     *
     * @param string $path Le nom de la constante.
     * @param string $rule Le nom de la règle à vérifier.
     * @param array $ruleParams Les paramètres à passer à la règle.
     * @return array
     */
    public function validateDefinePath($path, $rule, $ruleParams = [])
    {
        $value = constant($path);

        $message = $this->_validate($value, $rule, $ruleParams);

        return [
            'success' => is_null($message),
            'value' => var_export($value, true),
            'message' => $message,
        ];
    }

    /**
     * Lecture des chemins de configuration et validation (à minima, les
     * valeurs ne pourront pas être vides).
     *
     * @deprecated
     *
     * @param array $paths Les chemins à vérifier
     * @return array
     */
    public function defined(array $paths)
    {
        $return = [];
        $defaults = [
            'allowEmpty' => false,
//              'required' => true,
        ];

        foreach (Hash::normalize($paths) as $path => $rules) {
            if (!is_array($rules)) {
                $rules = [ [ 'rule' => $rules ] ];
            }

            foreach ($rules as $rule) {
                $rule = Hash::merge($defaults, $rule);
                if (!isset($return[$path]) || empty($return[$path]) || $return[$path]['success']) {
                    $ruleParams = $rule;
                    unset($ruleParams['rule']);
                    $validate = $this->validateDefinePath($path, $rule['rule'], $ruleParams);
                    $return[$path] = $validate;
                }
            }
        }
        ksort($return);

        return $return;
    }

    /**
     * Vérifie qu'un logiciel sse trouve dans une version donnée.
     *
     * @deprecated
     *
     * @param string $software Le nom du logiciel
     * @param string $actual La version du logiciel
     * @param string $low La version minimale
     * @param string $high La version maximale
     * @return array
     */
    public function version($software, $actual, $low, $high = null)
    {
        $difference = LibricielChecksVersion::difference($actual, $low, $high);

        $message = null;
        if (!$difference) {
            if (is_null($high)) {
                $message = "La version de {$software} doit être au moins {$low}";
            } else {
                $message = "La version de {$software} doit être au moins {$low}, mais plus petite que {$high}";
            }
        }

        return [
            'value' => $actual,
            'success' => $difference,
            'message' => $message,
        ];
    }

    /**
     * Retourne la vérification du timeout, avec en message la configuration
     * utilisée.
     *
     * @deprecated
     *
     * @fixme La fonction readTimeout n'est pas dans le plugin
     *
     * @return array
     */
    public function timeout()
    {
        $value = readTimeout();
        $message = null;

        if (Configure::read('Session.save') == 'php') {
            $message = '<code>session.gc_maxlifetime</code> dans le <code>php.ini</code> (valeur actuelle: <em>' . ini_get('session.gc_maxlifetime') . '</em> secondes)';
        } elseif (Configure::read('Session.save') == 'cake') {
            $message = "<code>Configure::write( 'Session.timeout', '" . Configure::read('Session.timeout') . "' )</code> dans <code>app/config/core.php</code><br/>";
            $message .= "<code>Configure::write( 'Security.level', '" . Configure::read('Security.level') . "' )</code> dans <code>app/config/core.php</code>";
        }

        return [
            'success' => true,
            'value' => sec2hms($value, true),
            'message' => $message,
        ];
    }

    /**
     * Convertit un nombre de secondes en un nombre de jours, heures, minutes
     * et secondes.
     *
     * @deprecated
     *
     * @url http://stackoverflow.com/posts/19680778/revisions
     *
     * @param int $seconds
     * @return string
     */
    protected function _secondsToTime($seconds)
    {
        $dtF = new DateTime("@0");
        $dtT = new DateTime("@$seconds");
        $msgid = '%a days, %h hours, %i minutes and %s seconds';

        return $dtF->diff($dtT)->format(__($msgid));
    }

    /**
     * Retourne les noms des caches utilisés ainsi que leur durée.
     *
     * @deprecated
     *
     * @return array
     */
    public function durations()
    {
        $results = [];
        $debug = Configure::read('debug');
        $min = 60 * 60 * 24;

        foreach (Cache::configured() as $key) {
            $seconds = Hash::get(Cache::config($key), 'settings.duration');
            $message = $this->_secondsToTime($seconds);
            $success = $debug || ( $seconds >= $min );

            if ($success === false) {
                $message .= sprintf(' au lieu de %s (%d)', $this->_secondsToTime($min), $min);
            }

            $results[$key] = [
                'value' => $seconds,
                'success' => $success,
                'message' => $message,
            ];
        }

        ksort($results);

        return $results;
    }

    /**
     * Vérifie l'accès à un WebService.
     *
     * @deprecated
     *
     * @param string $wsdl L'URL du webservice
     * @param array $params Un array de paramètres
     *  - "message": le gabarit du message à utiliser en cas d'erreur (par
     *               défaut: "Le WebService n'est pas accessible (%s)")
     *  - "timeout": la durée du timeout en secondes (par défaut: 30)
     * @return array
     */
    public function webservice($wsdl, array $params = [])
    {
        $params += [
            'message' => 'Le WebService n\'est pas accessible (%s)',
            'timeout' => 30,
        ];
        $result = [
            'success' => true,
            'message' => null,
            'value' => $wsdl,
        ];

        $oldTimeout = ini_get('default_socket_timeout');
        ini_set('default_socket_timeout', $params['timeout']);

        // Faire une méthode dans le Check ?
        $handle = @fopen($wsdl, 'r');
        if (!empty($handle)) {
            fclose($handle);
        } else {
            $error = error_get_last();
            $result['success'] = false;
            $result['message'] = sprintf($params['message'], $error['message']);
        }

        if ($result['success'] === true) {
            // INFO: si le wsdl ne répond pas, page blanche / erreur 500 !!
            try {
                $options = [
                    'connection_timeout' => $params['timeout'],
                    'exceptions' => true,
                    'trace' => false,
                    'cache_wsdl' => WSDL_CACHE_NONE,
                ];
                $client = @new SoapClient($wsdl, $options);
                $result['success'] = true;
            } catch (Exception $e) {
                $result['success'] = false;
                $result['message'] = sprintf($params['message'], trim($e->getMessage()));
            }
        }

        ini_set('default_socket_timeout', $oldTimeout);

        return $result;
    }

    /**
     * Vérifie l'accès à une machine distante.
     *
     * @see http://www.php.net/manual/fr/function.fsockopen.php#65631
     * @deprecated
     *
     * @param string $hostname
     * @param string $port
     * @param array $params Un array de paramètres
     *  - "message": le gabarit du message à utiliser en cas d'erreur (par
     *               défaut: "La machine distante n'est pas accessible (%s)")
     *  - "timeout": la durée du timeout en secondes (par défaut: 30)
     * @return array
     */
    public function socket($hostname, $port, array $params = [])
    {
        $params += [
            'message' => 'La machine distante n\'est pas accessible (%s)',
            'timeout' => 30,
        ];

        $result = [
            'success' => true,
            'message' => null,
            'value' => "{$hostname}:{$port}",
        ];

        $handle = @fsockopen($hostname, $port, $errno, $errstr, $params['timeout']);

        if (empty($handle) === false) {
            $result['success'] = true;
            fclose($handle);
        } else {
            $result['success'] = false;
            $result['message'] = sprintf($params['message'], "{$errno}: {$errstr}");
        }

        return $result;
    }

    /**
     * Vérifie la présence d'extensions PEAR
     *
     * @deprecated
     *
     * @param array $extensions
     * @param bool $base A true, vérifie la présence des classes PEAR et PEAR_Registry.
     * @return array
     */
    public function pearExtensions($extensions, $base = true)
    {
        $results = [];

        $success = (bool)@include_once 'PEAR.php';
        if ($base) {
            $results['PEAR'] = [
                'success' => $success,
                'message' => ( $success ? null : "PEAR n'est pas installé. Installez le paquet php-pear (sous Ubuntu, en ligne de commande, faire: <code>sudo apt-get install php-pear</code>)" ),
            ];
        }

        $success = (bool)@include_once 'PEAR/Registry.php';
        if ($base) {
            $results['Registry'] = [
                'success' => $success,
                'message' => ( $success ? null : "PEAR_Registry n'est pas installé" ),
            ];
        }

        $Registry = null;
        if (class_exists('PEAR_Registry')) {
            $Registry = @new PEAR_Registry();
        }

        foreach ($extensions as $extension) {
            $success = ( is_null($Registry) ? false : @$Registry->packageExists($extension) );
            $results[$extension] = [
                'success' => $success,
                'message' => ( $success ? null : sprintf("L'extension PEAR %s n'est pas installée. Pour l'installer, en ligne de commande, faire: <code>sudo pear install %s</code>", $extension, $extension) ),
            ];
        }

        return $results;
    }

    /**
     * Vérification d'une configuration de CakeEmail dans le fichier
     * app/Config/email.php.
     *
     * EmailComponent est déprécié depuis la version 2.0 de CakePHP.
     *
     * @deprecated
     *
     * @todo ::transportClass
     *
     * @param string $name Le nom de la configuration
     * @return array
     */
    public function cakeEmailConfig($name)
    {
        $settingsFile = 'app/Config/email.php';
        $settingsMailFields = [ 'from', 'replyTo', 'readReceipt', 'returnPath', 'sender', 'to', 'cc', 'bcc' ];

        $configure = [];
        $tests = [];

        try {
            $Email = new CakeEmail($name);
            $settings = $Email->config();
        } catch (Exception $e) {
            $Email = null;
            $settings = [];
        }

        if (empty($settings)) {
            $configure["Présence de la clé {$name} dans le fichier {$settingsFile}"] = [
                'success' => false,
                'message' => "Configuration manquante ou erronée dans le fichier {$settingsFile}",
            ];
        } else {
            // La configuration est-elle présente ?
            $configure[$settingsFile] = [
                'success' => true,
                'message' => null,
            ];

            // Vérification des adresses mail
            foreach ($settingsMailFields as $emailAddress) {
                if (isset($settings[$emailAddress]) && !empty($settings[$emailAddress])) {
                    foreach ((array)$settings[$emailAddress] as $address) {
                        $success = Validation::email($address, true);

                        $configure["{$emailAddress}.{$address}"] = [
                            'success' => $success,
                            'message' => ( $success ? null : "L'adresse mail {$address} n'est pas valide" ),
                        ];
                    }
                }
            }

            // Test de connexion au serveur SMTP
            if (!is_null($Email) && ( isset($settings['transport']) && strtolower($settings['transport']) == 'smtp' )) {
                $tests['Connexion au serveur'] = $this->socket($settings['host'], $settings['port']);
            }

            // Test d'envoi de mail factice
            $success = false;
            if (!is_null($Email)) {
                try {
                    $Email->subject('Test');
                    $Email->to($Email->from());
                    $Email->transport('Debug');
                    $result = $Email->send('Test');
                    $success = !empty($result);
                } catch (Exception $e) {
                    $success = false;
                }
            }
            $tests['Envoi factice'] = [
                'success' => $success,
                'message' => ( !empty($success) ? null : 'Erreur lors du test d\'envoi de mail factice' ),
            ];
        }

        return [ 'configure' => $configure, 'tests' => $tests ];
    }

    /**
     *
     * @deprecated
     *
     * @param string $modelName
     * @param string $configureKey
     * @param bool $array_keys
     * @return array
     */
    public function configurePrimaryKey($modelName, $configureKey, $array_keys = false)
    {
        $primaryKeys = (array)Configure::read($configureKey);
        if (empty($primaryKeys)) {
            return [
                'success' => false,
                'message' => "Aucune clé primaire n'est présente.",
            ];
        }

        $results = [];
        $model = ClassRegistry::init($modelName);

        if ($array_keys) {
            $primaryKeys = array_keys($primaryKeys);
        }

        foreach ($primaryKeys as $primaryKey) {
            $querydata = [
                'conditions' => [
                    "{$model->alias}.{$model->primaryKey}" => $primaryKey,
                ],
                'contain' => false,
            ];
            $results[$primaryKey] = ( $model->find('count', $querydata) == 1 );
        }

        if (!array_search(false, $results, true)) {
            return [
                'success' => true,
                'message' => null,
            ];
        } else {
            $missing = [];
            foreach ($results as $primaryKey => $result) {
                if (!$result) {
                    $missing[] = $primaryKey;
                }
            }
            $missing = implode(', ', $missing);
            $table = Inflector::tableize($model->name);

            return [
                'success' => false,
                'message' => "Les clés primaires suivantes sont manquantes dans la table {$table}: {$missing}",
            ];
        }
    }

    /**
     * Vérification de l'utilisateur et du groupe propriétaires des fichiers
     * et dossiers du cache, ainsi que des permissions sur ces fichiers.
     *
     * @deprecated
     *
     * @return array
     */
    public function cacheFilePermissions()
    {
        $return = [];
        $configNames = (array)Cache::configured();
        $user = posix_getpwuid(posix_geteuid());
        $group = posix_getgrgid($user['gid']);
        $uname = posix_uname();

        foreach ($configNames as $configName) {
            $config = Cache::config($configName);
            if (strtolower($config['engine']) === 'file') {
                $dir = $config['settings']['path'];
                if (strtolower($uname['sysname']) !== 'linux') {
                    $success = false;
                    $message = sprintf('Vérification du propriétaire et des permissions du dossier <var>%s</var> non prévue sur un système %s', $dir, $uname['sysname']);
                } else {
                    $files = [];
                    $return_var = 0;
                    $command = "find {$dir} -maxdepth 1 \\( -not -group {$group['name']} \\) -o \\( -not -user {$user['name']} \\) -o \\( -not -perm -u=rw -o -not -perm -g=rw \\) | sort";
                    exec($command, $files, $return_var);

                    if ($return_var == 0) {
                        $success = empty($files) === true;
                        $message = null;
                        if ($success === false) {
                            $message = "Des fichiers ou des dossiers du dossier <var>{$dir}</var> n'appartiennent pas au bon utilisateur ou groupe ou n'ont pas les bonnes permissions.<br/>";
                            $message .= "Il faudrait lancer la commande suivante pour corriger le problème: <code>sudo chown -R {$user['name']}:{$group['name']} {$dir} && sudo chmod o+rwX,g+rwX -R {$dir}</code><br/>";
                            $message .= "Les fichiers concernés sont: <br/><ul><li>" . implode("</li><li>", $files) . "</li></ul>";
                        }
                    } else {
                        $success = false;
                        $message = sprintf('Erreur lors de l\'exécution de la commande %s (%d)', $command, $return_var);
                    }
                }

                $return[$configName] = [
                    'success' => $success,
                    'message' => $message,
                ];
            }
        }

        return $return;
    }

    /**
     * Effectue des tests d'écriture, de lecture et de suppression du cache,
     * pour toutes les configurations définies.
     *
     * @deprecated
     *
     * @return array
     */
    public function cachePermissions()
    {
        $cacheKeyBase = self::class . '_' . __FUNCTION__;
        $value = time();
        $return = [];

        $savedCacheDisable = Configure::read('Cache.disable');
        Configure::write('Cache.disable', false);
        $savedDebug = Configure::read('debug');
        Configure::write('debug', 0);

        $configNames = Cache::configured();
        if (!empty($configNames)) {
            foreach ($configNames as $configName) {
                if (!Cache::isInitialized($configName)) {
                    Cache::config($configName);
                }

                $cacheKey = "{$cacheKeyBase}_{$configName}";

                $write = Cache::write($cacheKey, $value, $configName);
                $read = ( Cache::read($cacheKey, $configName) == $value );
                $delete = Cache::delete($cacheKey, $configName);

                $success = ( $write && $read && $delete );
                $message = null;

                if (!$success) {
                    $actions = [];
                    if (!$write) {
                        $actions[] = 'écriture';
                    }
                    if (!$read) {
                        $actions[] = 'lecture';
                    }
                    if (!$delete) {
                        $actions[] = 'suppression';
                    }
                    $actions = implode(', ', $actions);
                    $message = "Problème(s) rencontré(s) pour \"{$configName}\": {$actions}";

                    $config = Cache::config($configName);
                    if ($config['engine'] == 'File') {
                        $path = preg_replace('/^' . preg_quote(APP, '/') . '/', APP_DIR . DS, $config['settings']['path']);
                        $message .= "; vérifiez les droits sur le répertoire: {$path}";
                    }
                }

                $return[$configName] = [
                    'success' => $success,
                    'message' => $message,
                ];
            }
        }
        Configure::write('Cache.disable', $savedCacheDisable);
        Configure::write('debug', $savedDebug);

        return $return;
    }

    /**
     * Vérifie l'espace disponible (par-rapport au seuil) pour tous les
     * répertoires passés en paramètre.
     *
     * @deprecated
     *
     * @param string|array $directories Le ou les répertoires à vé"rifier.
     *  Il est possible de passer un array avec le chemin en clé et un array
     *  en valeur, contenant la clé threshold pour ce répertoire en particulier.
     * @param array $params La clé threshold permet de spécifier le nombre
     *   d'octets minimum requis (l'équivalent de 100 Mo par défaut).
     * @return array
     */
    public function freespace($directories, array $params = [])
    {
        $directories = Hash::normalize((array)$directories);
        $params += [ 'threshold' => 104857600 ];
        $return = [];

        foreach ($directories as $directory => $options) {
            $return[$directory] = [
                'success' => true,
                'message' => null,
            ];
            $options = (array)$options;
            $options += $params;

            if (is_dir($directory) === false) {
                $return[$directory]['success'] = false;
                $msgstr = 'Le répertoire %s n\'existe pas';
                $return[$directory]['message'] = sprintf($msgstr, $directory);
            } else {
                $freeBytes = disk_free_space($directory);

                if ($freeBytes < $options['threshold']) {
                    $return[$directory]['success'] = false;
                    $msgstr = 'L\'espace disponible dans le répertoire %s devrait être d\'au moins %s mais il ne reste actuellement que %s';
                    $return[$directory]['message'] = sprintf(
                        $msgstr,
                        $directory,
                        LibricielChecksBytesize::toHuman($options['threshold']),
                        LibricielChecksBytesize::toHuman($freeBytes)
                    );
                }
            }
        }

        return $return;
    }
}
