# LibricielChecks

Plugin CakePHP 2.x permettant de faciliter la vérification d'un application.

## Configuration

Du code CSS est présent dans le fichier `webroot/css/libriciel-checks.css` pour les accordéons utilisés dans des écrans du type "Vérification de l'application".

### Chargement du plugin

```php
CakePlugin::loadAll(
    [
        // ...
        'LibricielChecks' => ['bootstrap' => false, 'routes' => false],
        // ...
    ]
);
```

```bash
sudo -u www-data \
    vendors/cakephp/cakephp/lib/Cake/Console/cake \
        test LibricielChecks AllLibricielChecksTests \
        --verbose \
        --strict \
        --stderr \
        --debug \
        --coverage-html "app/tmp/coverage" \
        --configuration phpunit.xml \
        -app app
```
