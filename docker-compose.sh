#!/usr/bin/env bash

CLEAR="0"

# ----------------------------------------------------------------------------------------------------------------------
# "Library"
# ----------------------------------------------------------------------------------------------------------------------

set -o errexit
set -o nounset
set -o pipefail

# Internal constants

declare -r _blue_="\e[34m"
declare -r _cyan_="\e[36m"
declare -r _default_="\e[0m"
declare -r _green_="\e[32m"
declare -r _red_="\e[31m"

# Bootstrap

if [ "`getopt --longoptions debug -- d "$@" 2> /dev/null | grep "\(\-d\|\-\-debug\)"`" != "" ] ; then
    declare -r __DEBUG__=1
    set -o xtrace
else
    declare -r __DEBUG__=0
fi

declare -r __PID__="${$}"

declare -r __FILE__="$(realpath "${0}")"
declare -r __SCRIPT__="$(basename "${__FILE__}")"
declare -r __ROOT__="$(realpath "$(dirname "${__FILE__}")")"

printf "${_cyan_}Startup:${_default_} started process ${__PID__}\n\n"

# Error and exit handling: exit is trapped, as well as signals.
# If a __cleanup__ function exists, it will be called on signal or exit and the exit code will be passed as parameter.

__trap_signals__()
{
    local code="${?}"

    if [ ${code} -ne 0 ] ; then
        local signal=$((${code} - 128))
        local name="$(kill -l ${signal} 2>/dev/null)"
        >&2 printf "\nProcess ${__PID__} received SIG${name} (${signal}), exiting..."
    fi

}

__trap_exit__() {
    local code="${?}"

    if [ ${code} -eq 0 ] ; then
        printf "\n${_green_}Success:${_default_} process ${__PID__} exited normally\n"
    else
        >&2 printf "\n${_red_}Error:${_default_} process ${__PID__} exited with error code ${code}\n"
    fi

    # @todo: no cleanup should be done for SIGQUIT ?
    if [ "$(type -t __cleanup__)" = "function" ]; then
        __cleanup__
    fi
}

trap "__trap_signals__" SIGHUP SIGINT SIGQUIT SIGTERM
trap "__trap_exit__" EXIT

# ----------------------------------------------------------------------------------------------------------------------
# Custom code
# ----------------------------------------------------------------------------------------------------------------------

COMPOSE_FILE="${__ROOT__}/docker-compose.yml"
PROJECT_NAME="libricielchecks"
IMAGE_NAME="${PROJECT_NAME}-app"

#@todo: docker-compose up --build

__usage__()
{
    printf "NAME\n"
    printf "  %s\n" "$__SCRIPT__"
    printf "\nDESCRIPTION\n"
    printf "  Commandes courantes docker-compose pour le projet libricielchecks\n"
    printf "\nSYNOPSIS\n"
    printf "  %s [OPTION] [COMMAND]\n" "$__SCRIPT__"
    printf "\nCOMMANDS\n"
    printf "  app\t\tDémarrage de plugin en conteneurs, pour les tests\n"
    printf "\nOPTIONS\n"
    printf "  -h|--help\tAffiche cette aide\n"
    printf "  -c|--clear\tSupprime les images docker avant de les reconstruire\n"
    printf "\nEXEMPLES\n"
    printf "  %s -h\n" "$__SCRIPT__"
    printf "  %s app\n" "$__SCRIPT__"
    printf "  %s -c app\n" "$__SCRIPT__"
    printf "  %s --clear app\n" "$__SCRIPT__"
}

clear_images()
{
    local IMAGE_NAMES="$@"

    docker image rmi -f "${IMAGE_NAMES}" > /dev/null 2>&1 \
    && docker image prune -f

    return $?
}

app()
{
    ( \
        clear_images "${IMAGE_NAME}" \
        && docker-compose -f "${COMPOSE_FILE}" --project-name "${PROJECT_NAME}" down -v --remove-orphans \
        && docker-compose -f "${COMPOSE_FILE}" --project-name "${PROJECT_NAME}" up \
        && docker-compose -f "${COMPOSE_FILE}" --project-name "${PROJECT_NAME}" down -v --remove-orphans \
    )

    EXIT_STATUS=$?
    if [ $EXIT_STATUS -ne 0 ]
    then
        echo >&2  "Erreur(s) lors du lancement du docker plugin (exit $EXIT_STATUS)"
    fi

    return $EXIT_STATUS
}

# ----------------------------------------------------------------------------------------------------------------------
# Main function
# ----------------------------------------------------------------------------------------------------------------------

__main__()
{
    (
        opts=`getopt --longoptions clear,help -- ch "$@"` || ( >&2 __usage__ ; exit 1 )
        eval set -- "$opts"
        while true ; do
            case "$1" in
                -c|--clear)
                    CLEAR="1"
                    shift
                ;;
                -h|--help)
                    __usage__
                    exit 0
                ;;
                --)
                    shift
                    break
                ;;
                *)
                    >&2 __usage__
                    exit 1
                ;;
            esac
        done

        case "${1:-}" in
            clear)
                shift
                clear_images "${IMAGE_NAME}"
                exit $?
            ;;
            app)
                shift
                app
                exit $?
            ;;
            --)
                shift
                break
            ;;
            *)
                >&2 __usage__
                exit 1
            ;;
        esac

        exit 0
    )
}

__main__ "$@"
