FROM ubuntu:18.04

#-------------------------------------------------------------------------------
# Labels
#-------------------------------------------------------------------------------
LABEL maintainer="Christian Buffin <christian.buffin@libriciel.coop>"

#-------------------------------------------------------------------------------
# Variables d'environnement
#-------------------------------------------------------------------------------

ENV WWW_DIR="/var/www/html"
ENV APP_DIR="${WWW_DIR}/plugin"
ENV PLUGIN_DIR="${APP_DIR}/app/Plugin/LibricielChecks"
ENV COMPOSER_ALLOW_SUPERUSER=1
ENV DEBIAN_FRONTEND=noninteractive
ENV DOCKER_TIMEZONE=Europe/Paris
ENV TZ=$DOCKER_TIMEZONE

#-------------------------------------------------------------------------------
# Installation des paquets nécessaires
#-------------------------------------------------------------------------------

RUN ( \
    set -eux \
    && apt-get update \
    && apt-get install \
        --assume-yes \
        --no-install-recommends \
        --show-upgraded \
        ca-certificates=* \
        gettext=0.19.* \
        git=1:2.17.* \
        iptables=1.6.* \
        locales=2.27-* \
        netbase=5.4 \
        nginx=1.14.* \
        php-xdebug=2.6.* \
        php7.2=7.2.* \
        php7.2-cli=7.2.* \
        php7.2-curl=7.2.* \
        php7.2-fpm=7.2.* \
        php7.2-mbstring=7.2.* \
        php7.2-pgsql=7.2.* \
        php7.2-soap=7.2.* \
        php7.2-sqlite3=7.2.* \
        php7.2-xml=7.2.* \
        php7.2-zip=7.2.* \
        sudo=1.8.* \
        tzdata=* \
        unzip=6.0-* \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
)

#-------------------------------------------------------------------------------
# Configuration de PHP et de nginx
#-------------------------------------------------------------------------------

COPY docker/app/etc/nginx/sites-available/plugin /etc/nginx/sites-available/
COPY docker/app/usr/bin/composer-install.php /usr/bin/composer-install
COPY docker/app/usr/bin/docker-entrypoint.sh /usr/bin/docker-entrypoint.sh

RUN set -eux && locale-gen fr_FR.UTF-8
RUN set -eux && echo "date.timezone = \"${DOCKER_TIMEZONE}\";" > /etc/php/7.2/fpm/conf.d/10-date.ini
RUN set -eux && ln -snf /usr/share/zoneinfo/$TZ /etc/localtime
RUN set -eux && echo $TZ > /etc/timezone
RUN set -eux && dpkg-reconfigure tzdata
RUN set -eux && unlink /etc/nginx/sites-enabled/default
RUN set -eux && ln -s /etc/nginx/sites-available/plugin /etc/nginx/sites-enabled
RUN set -eux && adduser www-data sudo

#-------------------------------------------------------------------------------
# Installation de composer
#-------------------------------------------------------------------------------

WORKDIR /tmp

RUN set -eux && php /usr/bin/composer-install -v 1.8.4

#-------------------------------------------------------------------------------
# Installation de l'application
#-------------------------------------------------------------------------------

RUN set -eux && mkdir -p "${APP_DIR}"

WORKDIR ${APP_DIR}

COPY composer.json composer.lock ${APP_DIR}/
COPY cake_utils.sh phpcs.xml phpmd.xml phpunit.xml ${APP_DIR}/

RUN set -eux && composer install
RUN set -eux && sed -i "s/\$looksGood = .*;/\$looksGood = 'y';/g" "vendors/cakephp/cakephp/lib/Cake/Console/Command/Task/ProjectTask.php"
RUN set -eux && ln -s "vendors" "Vendor"
RUN set -eux && vendors/bin/cake bake project "${APP_DIR}/app" --empty --quiet
RUN set -eux && mkdir -p "${PLUGIN_DIR}"
RUN set -eux && rm "${WWW_DIR}/index.nginx-debian.html"

# phpunit/phpunit 3.7.38
RUN set -eux && sed -i "s/public function assertEquals(\$expected, \$actual, \$delta = 0, \$canonicalize = FALSE, \$ignoreCase = FALSE)/public function assertEquals(\$expected, \$actual, \$delta = 0, \$canonicalize = FALSE, \$ignoreCase = FALSE, array \&\$processed = array())/g" "vendors/phpunit/phpunit/PHPUnit/Framework/Comparator/DOMDocument.php"
RUN set -eux && sed -i "s/\$numTests = count(\$coverageData\[\$i\]);/\$numTests = count((array)\$coverageData[\$i]);/g" "vendors/phpunit/php-code-coverage/PHP/CodeCoverage/Report/HTML/Renderer/File.php"

COPY . "${PLUGIN_DIR}"

COPY "docker/app/var/www/html/plugin/app/Config/bootstrap.php" "${APP_DIR}/app/Config/bootstrap.php"
COPY "docker/app/var/www/html/plugin/app/Config/database.php" "${APP_DIR}/app/Config/database.php"
COPY "docker/app/var/www/html/plugin/app/Config/routes.php" "${APP_DIR}/app/Config/routes.php"

#-------------------------------------------------------------------------------
# Démarrage de l'application
#-------------------------------------------------------------------------------

ENTRYPOINT ["/usr/bin/docker-entrypoint.sh", "--start-services"]
