#!/usr/bin/php
<?php
$versions = [
    '1.8.4' => [
        'installer' => [
            'revision' => 'cb19f2aa3aeaa2006c0cd69a7ef011eb31463067',
            'sha384' => '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5'
        ],
        'composer' => [
            'sha384' => '4fc189769a44349c6c3830fc57888356cf77f0a31ded361eca37f654881e4fe318628ce8f7a84a58d83eff32f193ef02'
        ]
    ],
    '1.8.3' => [
        'installer' => [
            'revision' => 'da7be05fa1c9f68b9609726451d1aaac7dd832cb',
            'sha384' => '48e3236262b34d30969dca3c37281b3b4bbe3221bda826ac6a9a62d6444cdb0dcd0615698a5cbe587c3f0fe57a54d8f5'
        ],
        'composer' => [
            'sha384' => '4232a7ed9d1322ce6b8bb0e0a0ae62192c49393a7f9bcc070c0632c796e23338a160eb10fedc272e61509ed7883455bb'
        ]
    ],
    '1.8.2' => [
        'installer' => [
            'revision' => '6ec979e0a88b67b5b9cbd13524b2ad62aebd0aed',
            'sha384' => '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8'
        ],
        'composer' => [
            'sha384' => 'e82617f9724ca0dbc2e4548c8af89ade5fcfdf4c2fbb458cbcc894854c626da0b42f9dec19d918d10fb618870291e35e'
        ]
    ],
    '1.8.1' => [
        'installer' => [
            'revision' => 'b6cc868084f9d776c1d1713eb6cf2448f53928c6',
            'sha384' => '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8'
        ],
        'composer' => [
            'sha384' => 'f35b2574b9c99d3acfd614f79be54e15384aa09c1696c2df4bcc577f3672d93708f36c4eba9dc0fd1ba8764704615406'
        ]
    ],
    '1.8.0' => [
        'installer' => [
            'revision' => 'd3e09029468023aa4e9dcd165e9b6f43df0a9999',
            'sha384' => '93b54496392c062774670ac18b134c3b3a95e5a5e5c8f1a9f115f203b75bf9a129d5daa8ba6a13e2cc8a1da0806388a8'
        ],
        'composer' => [
            'sha384' => 'f74246a5bcc0f3aa997e856e3ba678a1d9ed5a9d093c1f51af3355dd91b92325b9117f236a317331e4012544c44f4ee9'
        ]
    ]
];

$options = [
    'f:' => 'filename:',
    'v:' => 'version:'
];
$params = getopt(implode('', array_keys($options)), array_values($options));

foreach ($options as $short => $long) {
    $short = trim($short, ':');
    $long = trim($long, ':');

    if (array_key_exists($short, $params) === true) {
        $params[$long] = $params[$short];
        unset($params[$short]);
    }
}

if (array_key_exists('version', $params) === false) {
    $version = array_keys($versions)[0];
} else {
    $version = $params['version'];
}

if (array_key_exists('filename', $params) === false) {
    $filename = '/usr/bin/composer';
} else {
    $filename = $params['filename'];
}

$installer = 'composer-setup.php';
$url = 'https://raw.githubusercontent.com/composer/getcomposer.org/%s/web/installer';
$success = true;

try {
    printf("Installing composer %s to %s\n", $version, $filename);
    
    if (array_key_exists($version, $versions) === false) {
        $msgstr = 'Version %s does not exist';
        throw new RuntimeException(sprintf($msgstr, $version));
    } else {
        $current = $versions[$version];
    }

    if (copy(sprintf($url, $current['installer']['revision']), $installer) === false) {
        $msgstr = 'Could not download file';
        throw new RuntimeException($msgstr);
    }

    $sha384 = hash_file('sha384', $installer);
    if ($sha384 !== $current['installer']['sha384']) {
        $msgstr = 'Installer corrupt (sha384): %s (expected %s)';
        throw new RuntimeException(sprintf($msgstr, $sha384, $current['installer']['sha384']));
    }

    $output = $return_var = null;
    $cmd = sprintf(
        "php %s --version=%s --install-dir=%s --filename=%s",
        $installer,
        $version,
        dirname($filename),
        basename($filename)
    );
    exec($cmd, $output, $return_var);
    unlink($installer);

    if ($return_var !== 0) {
        $msgstr = 'Install command failed (%d): %s';
        throw new RuntimeException(sprintf($msgstr, $return_var, var_export($output, true)));
    }
    
    $sha384 = hash_file('sha384', $filename);
    if ($sha384 !== $current['composer']['sha384']) {
        $msgstr = 'Composer corrupt (sha384): %s (expected %s)';
        throw new RuntimeException(sprintf($msgstr, $sha384, $current['composer']['sha384']));
    }

    exit(0);
} catch (\Throwable $thrown) {
    $error = sprintf(
        "%s:%d\n%s\n",
        $thrown->getFile(),
        $thrown->getLine(),
        $thrown->getMessage()
    );

    fwrite(STDERR, $error);

    @unlink($installer);
    exit(1);
}
