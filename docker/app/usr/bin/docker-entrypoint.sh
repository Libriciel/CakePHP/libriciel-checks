#!/usr/bin/env bash

# ----------------------------------------------------------------------------------------------------------------------
# "Library"
# ----------------------------------------------------------------------------------------------------------------------

set -o errexit
set -o nounset
set -o pipefail

# Internal constants

declare -r _blue_="\e[34m"
declare -r _cyan_="\e[36m"
declare -r _default_="\e[0m"
declare -r _green_="\e[32m"
declare -r _red_="\e[31m"

# Bootstrap

if [ "`getopt --longoptions debug -- d "$@" 2> /dev/null | grep "\(\-d\|\-\-debug\)"`" != "" ] ; then
    declare -r __DEBUG__=1
    set -o xtrace
else
    declare -r __DEBUG__=0
fi

declare -r __PID__="${$}"

declare -r __FILE__="$(realpath "${0}")"
declare -r __SCRIPT__="$(basename "${__FILE__}")"
declare -r __ROOT__="$(realpath "$(dirname "${__FILE__}")")"

printf "${_cyan_}Startup:${_default_} started process ${__PID__}\n\n"

# Error and exit handling: exit is trapped, as well as signals.
# If a __cleanup__ function exists, it will be called on signal or exit and the exit code will be passed as parameter.

__trap_signals__()
{
    local code="${?}"

    if [ ${code} -ne 0 ] ; then
        local signal=$((${code} - 128))
        local name="$(kill -l ${signal} 2>/dev/null)"
        >&2 printf "\nProcess ${__PID__} received SIG${name} (${signal}), exiting..."
    fi

}

__trap_exit__() {
    local code="${?}"

    if [ ${code} -eq 0 ] ; then
        printf "\n${_green_}Success:${_default_} process ${__PID__} exited normally\n"
    else
        >&2 printf "\n${_red_}Error:${_default_} process ${__PID__} exited with error code ${code}\n"
    fi

    # @todo: no cleanup should be done for SIGQUIT ?
    if [ "$(type -t __cleanup__)" = "function" ]; then
        __cleanup__
    fi
}

trap "__trap_signals__" SIGHUP SIGINT SIGQUIT SIGTERM
trap "__trap_exit__" EXIT

#-----------------------------------------------------------------------------------------------------------------------

printf "%s: démarrage des services...\n" "$__SCRIPT__" \
&& service php7.2-fpm start \
&& nginx -g 'daemon off;'
